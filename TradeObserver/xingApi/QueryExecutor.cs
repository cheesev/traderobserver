﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using XA_DATASETLib;
using XA_SESSIONLib;
using TradeObserver.xingApi.TR;
using TradeObserver.dto.stock;
using TradeObserver.common.service;
using TradeObserver.xingApi.REAL;
using TradeObserver.common.message;
using TradeObserver.common.dto;
using TradeObserver.common.util;
using TradeObserver.common.factory;
using TradeObserver.common.contants;

namespace TradeObserver
{
    class QueryExecutor
    {
        [DllImport("kernel32.dll")]
        static extern uint GetCurrentThreadId();

        [DllImport("user32.dll")]
        static extern bool PostThreadMessage(uint idThread, uint Msg, UIntPtr wParam, IntPtr lParam);

        private const int MESSAGE_TYPE_QUIT = 9999;
        private const int MESSAGE_LOGIN = 10000;
        private const int MESSAGE_T1310 = 1310;
        private const int MESSAGE_T1702 = 1702;
        private const int MESSAGE_T8430 = 8430;
        private const int MESSAGE_T0424 = 0424;
        private const int MESSAGE_T0425 = 0425;
        private const int MESSAGE_CSPAT00600 = 00600;
        private const int MESSAGE_CDPCQ04700 = 04700;

        private const int MESSAGE_MARKET_REAL = 5000;

        private const int MESSAGE_t1305 = 1305;

        //private const int MESSAGE_CSPAQ02200 = 10001;
        private const int MESSAGE_CSPAQ02300 = 10002;
        private const int MESSAGE_FCOCCQ33600 = 33600;



        private const int MESSAGE_t2105 = 10005;

        private const int MESSAGE_t1511 = 10007;

        /// <summary>
        /// This class will run our delegate.
        /// </summary>
        private class MyMessageFilter : IMessageFilter
        {
            private readonly int m_messageId;
            private readonly ThreadStart m_dlgt;
            /// <summary>
            /// Creates a filter which runs the specified delegate when it encounters the specified message id.
            /// </summary>
            /// <param name="messageId">Message ID to look for</param>
            /// <param name="dlgt">Delegate to run upon message</param>
            public MyMessageFilter(int messageId, ThreadStart dlgt)
            {
                m_messageId = messageId;
                m_dlgt = dlgt;
            }
            public bool PreFilterMessage(ref Message m)
            {
                if (m.Msg == m_messageId)
                {
                    m_dlgt();
                    return true;//got the message. no one else need to process the message
                }
                else
                    return false;//not mine.
            }
        }
        /// <summary>
        /// Will hold the ID of the message loop
        /// </summary>
        private static uint id;
        public static bool isRunning = true;
        private ApplicationContext threadContext = null;
        public Dictionary<string, string> InputDataTable = new Dictionary<string, string>();

        public static Dictionary<string, object> sharedObject = new Dictionary<string, object>();
        // singleton - 멀티스레드환경에서 sync를 해줘야하지만 여기선 그렇게하지 않음.
        private XASessionClass myXASessionClass = new XASessionClass();

        public void startTr(string trCode)
        {
            //parameters = inParams;
            //myXASessionClass = XA;
            myXASessionClass._IXASessionEvents_Event_Login += new XA_SESSIONLib._IXASessionEvents_LoginEventHandler(myXASessionClass__IXASessionEvents_Event_Login);
            myXASessionClass.Disconnect += new XA_SESSIONLib._IXASessionEvents_DisconnectEventHandler(myXASessionClass_Disconnect);

            //creating a thread which will run the .NET message loop.
            Thread messageLoopThread = new Thread(MessageLoopThreadFunction);
            messageLoopThread.Name = "MessageLoopThread";
            messageLoopThread.SetApartmentState(ApartmentState.MTA);//important?
            messageLoopThread.Start();
            Thread.Sleep(400);
            isRunning = true;
            //switch (((QueryVO)QueryExcuter.Parameters["queryVO"]).trCode)
            switch (trCode)
            {
                case "login":
                    PostThreadMessage(id, MESSAGE_LOGIN, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "logout":
                    PostThreadMessage(id, MESSAGE_TYPE_QUIT, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "t1310":
                    PostThreadMessage(id, MESSAGE_T1310, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "t1702":
                    PostThreadMessage(id, MESSAGE_T1702, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "t8430":
                    PostThreadMessage(id, MESSAGE_T8430, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "CSPAT00600": // 현물주문
                    PostThreadMessage(id, MESSAGE_CSPAT00600, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "CDPCQ04700": // 계좌 거래내역 조회
                    PostThreadMessage(id, MESSAGE_CDPCQ04700, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "MARKET_REAL": // 주요 종목의 실시간 정보조회
                    PostThreadMessage(id, MESSAGE_MARKET_REAL, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "t0424": //주식 잔고조회
                    PostThreadMessage(id, MESSAGE_T0424, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "t0425": //주식체결/미체결
                    PostThreadMessage(id, MESSAGE_T0425, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "t1305": //기간별주가
                    PostThreadMessage(id, MESSAGE_t1305, UIntPtr.Zero, IntPtr.Zero);
                    break;
                case "FCOCCQ33600": //주식계좌 기간별수익률 상세
                    PostThreadMessage(id, MESSAGE_FCOCCQ33600, UIntPtr.Zero, IntPtr.Zero);
                    break;

                /*
            case "CSPAQ02200": //현물계좌예수금 주문가능금액 총평가조회
                PostThreadMessage(id, MESSAGE_CSPAQ02200, UIntPtr.Zero, IntPtr.Zero);
                break;
            case "CSPAQ02300": //잔고 조회
                PostThreadMessage(id, MESSAGE_CSPAQ02300, UIntPtr.Zero, IntPtr.Zero);
                break;
            case "t0425": //주식체결/미체결
                PostThreadMessage(id, MESSAGE_t0425, UIntPtr.Zero, IntPtr.Zero);
                break;
                
            case "t2105": //선물/옵션현재가호가조회
                PostThreadMessage(id, MESSAGE_t2105, UIntPtr.Zero, IntPtr.Zero);
                break;
            case "t1511": // 업종현재가
                PostThreadMessage(id, MESSAGE_t1511, UIntPtr.Zero, IntPtr.Zero);
                break;
            case "CDPCQ04700": // 계좌거래내역
                PostThreadMessage(id, MESSAGE_CDPCQ04700, UIntPtr.Zero, IntPtr.Zero);
                break; 
                */
            }
            while (isRunning)
            {
                if(trCode.EndsWith("REAL"))
                {
                    Thread.Sleep(10);
                }
                else
                {
                    Thread.Sleep(300);
                }
            }

            // 쓰레드 종료
            //myXASessionClass.DisconnectServer();            
            //messageLoopThread.Abort();
            threadContext.ExitThread();
            threadContext = null;
            //            parameters.Remove("resultList");
        }

        private void MessageLoopThreadFunction()
        {
            //getting my thread ID
            id = GetCurrentThreadId();
            //creating filters and delegate to run
            /*  inputDataTable Data
            **  server , loginId , loginPwd, pkiPwd - 공인인증서
            */
            IMessageFilter loginFilter = new MyMessageFilter(MESSAGE_LOGIN, delegate()
            {
                if (myXASessionClass.ConnectServer(InputDataTable["server"], 20001))//실계좌 서버 주소 및 포트
                {
                    //if (myXASessionClass.Login(queryVO.getLoginId(), queryVO.getLoginPwd(), queryVO.getPkiPwd(), 0, false))
                    if (myXASessionClass.Login(InputDataTable["loginId"], InputDataTable["loginPwd"], InputDataTable["pkiPwd"], 0, false))
                        Debug.WriteLine("login request sended");
                    else
                        Debug.WriteLine("login request fail");
                }
                else
                    Debug.WriteLine("cannot login to server");
            });
            IMessageFilter logoutFilter = new MyMessageFilter(MESSAGE_TYPE_QUIT, delegate()
            {
                myXASessionClass.Logout();
                myXASessionClass.DisconnectServer();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            });
            IMessageFilter t1310Filter = new MyMessageFilter(MESSAGE_T1310, delegate()
            {
                Thread.Sleep(500);
                sharedObject["ContractStrengthByTimeDtoList"] = new List<ContractStrengthByTimeDto>();
                t1310 myT1310 = new t1310();
                myT1310.CallBackMethod = ReceiveData_t1310;
                myT1310.QueryExcute(InputDataTable);
            });
            IMessageFilter t1702Filter = new MyMessageFilter(MESSAGE_T1702, delegate()
            {
                Thread.Sleep(500);
                t1702 myT1702 = new t1702();
                myT1702.CallBackMethod = ReceiveData_t1702;
                myT1702.QueryExcute(InputDataTable);
            });
            IMessageFilter t8430Filter = new MyMessageFilter(MESSAGE_T8430, delegate()
            {
                t8430 myT8430 = new t8430();
                myT8430.CallBackMethod = ReceiveData_t8430;
                myT8430.QueryExcute(InputDataTable);
            });
            IMessageFilter t0424Filter = new MyMessageFilter(MESSAGE_T0424, delegate()
            {
                t0424 myT0424 = new t0424();
                myT0424.CallBackMethod = ReceiveData_t0424;
                myT0424.QueryExcute(InputDataTable);
            });

            IMessageFilter MARKET_REAL_Filter = new MyMessageFilter(MESSAGE_MARKET_REAL, delegate()
            {
                Debug.WriteLine("MARKET_REAL_Filter");
                S3_ kospiReal = new S3_();       //kospi
                K3_ kosdaqReal = new K3_();      //kosdaq
                kosdaqReal.CallBackMethod += ReceiveRealData_myStockReal;
                kospiReal.CallBackMethod += ReceiveRealData_myStockReal;
                int idx = 0;
                //index로 구분되어 0은 코스피, 1은 코스닥.. 이런식으로..
                Dictionary<string, string> localDataTable = new Dictionary<string, string>();
                foreach (string stockCode in InputDataTable["myKospiStock"].Split(','))
                {
                    if (!string.IsNullOrEmpty(stockCode))
                    {
                        localDataTable["shcode"] = stockCode;
                        kospiReal.AdviseExcute(localDataTable);
                        Debug.WriteLine("advise kospi success : " + stockCode);
                    }
                }

                foreach (string stockCode in InputDataTable["myKosdaqStock"].Split(','))
                {
                    if (!string.IsNullOrEmpty(stockCode))
                    {
                        localDataTable["shcode"] = stockCode;
                        kosdaqReal.AdviseExcute(localDataTable);
                        Debug.WriteLine("advise kosdaq success : " + stockCode);
                    }
                }
            });

            IMessageFilter CSPAT00600Filter = new MyMessageFilter(MESSAGE_CSPAT00600, delegate()
            {
                CSPAT00600 myCSPAT00600 = new CSPAT00600();
                myCSPAT00600.CallBackMethod = ReceiveData_CSPAT00600;
                myCSPAT00600.QueryExcute(InputDataTable);
            });

            IMessageFilter CDPCQ04700Filter = new MyMessageFilter(MESSAGE_CDPCQ04700, delegate()
            {
                CDPCQ04700 myCDPCQ04700 = new CDPCQ04700();
                myCDPCQ04700.CallBackMethod = ReceiveData_CDPCQ04700;
                myCDPCQ04700.QueryExcute(InputDataTable);

            });
            IMessageFilter t1305Filter = new MyMessageFilter(MESSAGE_t1305, delegate()
            {
                t1305 myT1305 = new t1305();
                myT1305.CallBackMethod = ReceiveData_t1305;
                myT1305.QueryExcute(InputDataTable);
            });

            IMessageFilter FCOCCQ33600Filter = new MyMessageFilter(MESSAGE_FCOCCQ33600, delegate()
            {
                FOCCQ33600 fOCCQ33600 = new FOCCQ33600();
                fOCCQ33600.CallBackMethod = ReceiveData_FOCCQ33600;
                fOCCQ33600.QueryExcute(InputDataTable);
            });



            /*
            IMessageFilter CSPAQ02200Filter = new MyMessageFilter(MESSAGE_CSPAQ02200, delegate()
            {
                QueryVO queryVO = (QueryVO)parameters["queryVO"];
                if (myXASessionClass.IsConnected())//실계좌 서버 주소 및 포트
                {
                    Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                    InputDataTable.Add("RecCnt", "1");
                    InputDataTable.Add("AcntNo", queryVO.getAcntNum());
                    InputDataTable.Add("Pwd", queryVO.getPwd());
                    Query myCSPAQ02200 = new Query("CSPAQ02200");
                    myCSPAQ02200.CallBackMethod = ReceiveData_CSPAQ02200;
                    myCSPAQ02200.CallBackMsg = ReceiveMsg;
                    myCSPAQ02200.QueryExcute(InputDataTable);

                }
                else
                {
                    parameters.Remove("isConnected");
                    parameters.Add("isConnected", false);
                    parameters.Remove("isLogin");
                    parameters.Add("isLogin", false);
                }
            });
            IMessageFilter CSPAQ02300Filter = new MyMessageFilter(MESSAGE_CSPAQ02300, delegate()
            {
                QueryVO queryVO = (QueryVO)parameters["queryVO"];
                if (myXASessionClass.IsConnected())//실계좌 서버 주소 및 포트
                {
                    Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                    InputDataTable.Add("RecCnt", "1");
                    InputDataTable.Add("AcntNo", queryVO.getAcntNum());
                    InputDataTable.Add("Pwd", queryVO.getPwd());
                    Query myCSPAQ02300 = new Query("CSPAQ02300");
                    myCSPAQ02300.CallBackMethod = ReceiveData_CSPAQ02300;
                    myCSPAQ02300.QueryExcute(InputDataTable);

                }
                else
                {
                    parameters.Remove("isConnected");
                    parameters.Add("isConnected", false);
                    parameters.Remove("isLogin");
                    parameters.Add("isLogin", false);
                }
            });
            */
            /*
            IMessageFilter t0425Filter = new MyMessageFilter(MESSAGE_T0425, delegate()
            {
                QueryVO queryVO = (QueryVO)parameters["queryVO"];
                if (myXASessionClass.IsConnected())//실계좌 서버 주소 및 포트
                {
                    Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                    InputDataTable.Add("accno", queryVO.getAcntNum());
                    InputDataTable.Add("passwd", queryVO.getPwd());
                    InputDataTable.Add("chegb", "0");
                    InputDataTable.Add("medosu", "0");
                    InputDataTable.Add("sortgb", "2");
                    InputDataTable.Add("cts_ordno", "");
                    CommonQuery myt0425 = new CommonQuery("t0425");
                    myt0425.CallBackMethod = ReceiveData_t0425;
                    myt0425.QueryExcute(InputDataTable);
                }
            });
            
            IMessageFilter t2105Filter = new MyMessageFilter(MESSAGE_t2105, delegate()
            {
                QueryVO queryVO = (QueryVO)parameters["queryVO"];
                if (myXASessionClass.IsConnected())//실계좌 서버 주소 및 포트
                {
                    Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                    InputDataTable.Add("shcode", queryVO.getShCode());
                    Query myt2105 = new Query("t2105");
                    myt2105.CallBackMethod = ReceiveData_t2105;
                    myt2105.QueryExcute(InputDataTable);
                }
                else
                {
                    parameters.Remove("isConnected");
                    parameters.Add("isConnected", false);
                    parameters.Remove("isLogin");
                    parameters.Add("isLogin", false);
                }
            });
            IMessageFilter t1511Filter = new MyMessageFilter(MESSAGE_t1511, delegate()
            {
                QueryVO queryVO = (QueryVO)parameters["queryVO"];
                if (myXASessionClass.IsConnected())//실계좌 서버 주소 및 포트
                {
                    Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                    InputDataTable.Add("upcode", queryVO.getUpCode());
                    Query myt1511 = new Query("t1511");
                    myt1511.CallBackMethod = ReceiveData_t1511;
                    myt1511.QueryExcute(InputDataTable);
                }
                else
                {
                    parameters.Remove("isConnected");
                    parameters.Add("isConnected", false);
                    parameters.Remove("isLogin");
                    parameters.Add("isLogin", false);
                }                
            });
            IMessageFilter CDPCQ04700Filter = new MyMessageFilter(MESSAGE_CDPCQ04700, delegate()
            {
                QueryVO queryVO = (QueryVO)parameters["queryVO"];
                if (myXASessionClass.IsConnected())//실계좌 서버 주소 및 포트
                {
                    Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                    InputDataTable.Add("RecCnt", "1");
                    InputDataTable.Add("AcntNo", queryVO.getAcntNum());
                    InputDataTable.Add("Pwd", queryVO.getPwd());
                    InputDataTable.Add("QrySrtDt", toDay);
                    InputDataTable.Add("QryEndDt", toDay);
                    Query myCDPCQ04700 = new Query("CDPCQ04700");
                    myCDPCQ04700.CallBackMethod = ReceiveData_CDPCQ04700;
                    myCDPCQ04700.QueryExcute(InputDataTable);

                }
                else
                {
                    parameters.Remove("isConnected");
                    parameters.Add("isConnected", false);
                    parameters.Remove("isLogin");
                    parameters.Add("isLogin", false);
                }
            });
            */
            //adding filters to the message loop
            Application.AddMessageFilter(loginFilter);
            Application.AddMessageFilter(logoutFilter);
            Application.AddMessageFilter(t1310Filter);
            Application.AddMessageFilter(t1702Filter);
            Application.AddMessageFilter(t0424Filter);
            Application.AddMessageFilter(t8430Filter);
            Application.AddMessageFilter(CSPAT00600Filter);
            Application.AddMessageFilter(MARKET_REAL_Filter);
            Application.AddMessageFilter(CDPCQ04700Filter);
            Application.AddMessageFilter(t1305Filter);
            Application.AddMessageFilter(FCOCCQ33600Filter);
            /*
            Application.AddMessageFilter(CSPAQ02200Filter);
            Application.AddMessageFilter(CSPAQ02300Filter);
            Application.AddMessageFilter(t0425Filter);
            
            Application.AddMessageFilter(t2105Filter);
            Application.AddMessageFilter(t1511Filter);
            
            */
            //running the message loop. This call will halt till the call to Application.ExitThread()
            threadContext = new ApplicationContext();
            Application.Run(threadContext);
        }


        // 세션끝기는 콜백
        private void myXASessionClass_Disconnect()
        {
            Debug.WriteLine("연결종료");
            isRunning = false;
        }
        // 로그인 콜백
        private void myXASessionClass__IXASessionEvents_Event_Login(string szCode, string szMsg)
        {
            Debug.WriteLine("login sucess : " + szMsg);
            isRunning = false;
        }
        private void ReceiveData_CSPAT00600(XAQueryClass myXAQueryClass)
        {
            string outBlock2 = "CSPAT00600OutBlock2";

            Debug.WriteLine(myXAQueryClass.GetFieldData(outBlock2, "OrdNo", 0));
            Debug.WriteLine(myXAQueryClass.GetFieldData(outBlock2, "OrdTime", 0));
            Debug.WriteLine(myXAQueryClass.GetFieldData(outBlock2, "OrdAmt", 0));
            Debug.WriteLine(myXAQueryClass.GetFieldData(outBlock2, "AcntNm", 0));
            Debug.WriteLine(myXAQueryClass.GetFieldData(outBlock2, "IsuNm", 0));

            isRunning = false;
        }
        /*
        private void ReceiveMsg(bool bIsSystemError, string nMessageCode, string szMessage)
        {
            Debug.Out.WriteLine(szMessage);
        }
        */
        // CDPCQ04700 콜백
        private void ReceiveData_t1310(XAQueryClass myXAQueryClass)
        {
            Debug.WriteLine("t1310 receive call");
            string outBlock_occur = "t1310OutBlock1";
            int cnt_t1310OutBlock1 = myXAQueryClass.GetBlockCount(outBlock_occur);
            for (int i = 0; i < cnt_t1310OutBlock1; i++)
            {
                ContractStrengthByTimeDto conStrTimeDto = new ContractStrengthByTimeDto();
                conStrTimeDto.stockCode = sharedObject["stockCode"].ToString();
                conStrTimeDto.contractTime = myXAQueryClass.GetFieldData(outBlock_occur, "chetime", i);
                conStrTimeDto.curPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "price", i));
                conStrTimeDto.fluctuation = double.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "diff", i));
                conStrTimeDto.contractStrength = double.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "chdegree", i));
                conStrTimeDto.volume = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "volume", i));
                ((List<ContractStrengthByTimeDto>)sharedObject["ContractStrengthByTimeDtoList"]).Add(conStrTimeDto);
                Debug.WriteLine("time : " + conStrTimeDto.contractTime);
            }
        }

        private void ReceiveData_t1702(XAQueryClass myXAQueryClass)
        {
            Debug.WriteLine("t1702 called");
            List<StockTradeSituationByDayDto> stockTradeSituationByDayDtoList = new List<StockTradeSituationByDayDto>();
            string outBlock_occur = "t1702OutBlock1";
            int cnt_t1702OutBlock1 = myXAQueryClass.GetBlockCount(outBlock_occur);
            for (int i = 0; i < cnt_t1702OutBlock1; i++)
            {
                //Debug.WriteLine(this.InputDataTable["shcode"] + " , " + myXAQueryClass.GetFieldData(outBlock_occur, "date", i));
                if (QueryExecutor.sharedObject["maxDate"] == null
                    || int.Parse(QueryExecutor.sharedObject["maxDate"].ToString()) < int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "date", i)))
                {   /* 테이블의 maxDate 보다 큰 것만 넣는다 */
                    StockTradeSituationByDayDto stockTradeSituationByDayDto = new StockTradeSituationByDayDto();
                    stockTradeSituationByDayDto.stockCode = this.InputDataTable["shcode"];
                    stockTradeSituationByDayDto.ymdDay = myXAQueryClass.GetFieldData(outBlock_occur, "date", i);
                    stockTradeSituationByDayDto.price = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "close", i));
                    stockTradeSituationByDayDto.diff = double.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "diff", i));
                    stockTradeSituationByDayDto.ant = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0008", i));
                    stockTradeSituationByDayDto.foreigner = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0009", i));
                    stockTradeSituationByDayDto.institution = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0018", i));
                    stockTradeSituationByDayDto.financialInvestment = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0001", i)); // 증권..이라고나옴.확인필요
                    stockTradeSituationByDayDto.trust = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0003", i));
                    stockTradeSituationByDayDto.assurance = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0002", i));
                    stockTradeSituationByDayDto.bank = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0004", i));
                    stockTradeSituationByDayDto.investmentBank = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0005", i));
                    stockTradeSituationByDayDto.pension = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0006", i));
                    stockTradeSituationByDayDto.privateEquityFund = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0000", i));
                    stockTradeSituationByDayDto.innerForeigner = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0010", i));
                    stockTradeSituationByDayDto.etc = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "amt0099", i));

                    stockTradeSituationByDayDtoList.Add(stockTradeSituationByDayDto);
                }
                else
                {
                    DBService.insertTransaction("insertStockTradeSituationByDay", stockTradeSituationByDayDtoList);
                    stockTradeSituationByDayDtoList.Clear();
                    isRunning = false;
                }
            }
            DBService.insertTransaction("insertStockTradeSituationByDay", stockTradeSituationByDayDtoList);
        }
        private void ReceiveData_t8430(XAQueryClass myXAQueryClass)
        {
            Debug.WriteLine("t8430 receive call");
            List<StockInfoDto> stockInfoDtoList = new List<StockInfoDto>();
            string outBlock_occur = "t8430OutBlock";
            int cnt_t8430OutBlock1 = myXAQueryClass.GetBlockCount(outBlock_occur);
            for (int i = 0; i < cnt_t8430OutBlock1; i++)
            {
                StockInfoDto stockInfoDto = new StockInfoDto();
                stockInfoDto.stockCode = myXAQueryClass.GetFieldData(outBlock_occur, "shcode", i);
                stockInfoDto.stockName = myXAQueryClass.GetFieldData(outBlock_occur, "hname", i);
                stockInfoDto.etfgubun = myXAQueryClass.GetFieldData(outBlock_occur, "etfgubun", i);
                stockInfoDto.gubun = myXAQueryClass.GetFieldData(outBlock_occur, "gubun", i);
                stockInfoDto.price = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "recprice", i));
                //((List<StockInfoDto>)sharedObject["StockInfoDtoList"]).Add(stockInfoDto);
                stockInfoDtoList.Add(stockInfoDto);
            }
            DBService.insertTransaction("insertStockInfo", stockInfoDtoList);
            isRunning = false;
        }

        private void ReceiveData_t0424(XAQueryClass myXAQueryClass)
        {
            Console.WriteLine("계좌 잔고 조회...called");
            List<StockCurrentAccountDto> stockCurrentAccountList = new List<StockCurrentAccountDto>();
            List<StockInfoDto> stockInfoList = new List<StockInfoDto>();

            string outBlock_occur1 = "t0424OutBlock1";
            int cnt_t0424OutBlock1 = myXAQueryClass.GetBlockCount(outBlock_occur1);
            for (int i = 0; i < cnt_t0424OutBlock1; i++)
            {
                StockCurrentAccountDto stockCurrentAccount = new StockCurrentAccountDto();
                StockInfoDto stockInfoDto = new StockInfoDto();
                stockInfoDto.stockCode = myXAQueryClass.GetFieldData(outBlock_occur1, "expcode", i);
                stockInfoDto.note = "CURRENT";
                stockCurrentAccount.stockCode = myXAQueryClass.GetFieldData(outBlock_occur1, "expcode", i);
                stockCurrentAccount.accountNum = InputDataTable["accno"];
                stockCurrentAccount.balanceAmount = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "janqty", i));
                stockCurrentAccount.avgPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "pamt", i));
                stockCurrentAccount.buyPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "mamt", i));
                stockCurrentAccount.currentPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "price", i));
                stockCurrentAccount.assessmentPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "appamt", i));
                stockCurrentAccount.assessmentProfitLoss = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "dtsunik", i));
                stockCurrentAccount.investmentProportion = double.Parse(myXAQueryClass.GetFieldData(outBlock_occur1, "sunikrt", i));

                stockCurrentAccountList.Add(stockCurrentAccount);
                stockInfoList.Add(stockInfoDto);
            }
            DBService.insertTransaction("upsertStockCurrentAccount", stockCurrentAccountList);

            DaoFactory.getInstance.Update("updateStockInfoToInitNote", null);    // 정보를 초기화하고 업데이트한다.
            DBService.updateTransaction("updateStockInfo", stockInfoList);       // 현재 계좌는 CURRENT라고 추가해준다..


            isRunning = false;
        }

        private void ReceiveData_t1305(XAQueryClass myXAQueryClass)
        {
            Console.WriteLine("기간별 주가 조회 (" + InputDataTable["shcode"] + " , " + InputDataTable["date"] + ")"); /* 현재 특정일자 조회(단건)용으로 쓰임 */
            string outBlock_occur = "t1305OutBlock1";
            int blockCount = myXAQueryClass.GetBlockCount(outBlock_occur);
            for (int i = 0; i < blockCount; i++)
            {
                StockPriceInfoDto stockPriceInfoDto = new StockPriceInfoDto();
                stockPriceInfoDto.ymdDay = myXAQueryClass.GetFieldData(outBlock_occur, "date", i);
                stockPriceInfoDto.stockCode = InputDataTable["shcode"];
                stockPriceInfoDto.endPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "close", i));
                stockPriceInfoDto.lowPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "low", i));
                stockPriceInfoDto.highPrice = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "high", i));
                stockPriceInfoDto.marketValue = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "marketcap", i));

                if ("updateAllStockInfo".Equals(XingFactory.getInstance.commonInfoDto.currentOpName))
                {
                    DaoFactory.getInstance.Update("updateStockInfoPrice", stockPriceInfoDto);
                }
                else if (myXAQueryClass.GetFieldData(outBlock_occur, "date", i).Equals(InputDataTable["date"]))
                {
                    DaoFactory.getInstance.Update("updateStockTradeHistoryPriceInfo", stockPriceInfoDto);
                    break; //단건만 하니 한개 업데이트 되면 나감..
                }

            }

            isRunning = false;
        }

        private void ReceiveData_CDPCQ04700(XAQueryClass myXAQueryClass)
        {
            Console.WriteLine("계좌거래 내역 조회 Start");

            string outBlock_occur = "CDPCQ04700OutBlock3";
            int cnt_t8430OutBlock1 = myXAQueryClass.GetBlockCount(outBlock_occur);
            int blockCount = myXAQueryClass.GetBlockCount(outBlock_occur);

            List<StockTradeHistoryDto> StockTradeHistoryDtoList = new List<StockTradeHistoryDto>();
            for (int i = 0; i < blockCount; i++)
            {
                StockTradeHistoryDto tradeHistoryDto = new StockTradeHistoryDto();
                tradeHistoryDto.ymdDay = myXAQueryClass.GetFieldData(outBlock_occur, "TrdDt", i);
                tradeHistoryDto.tradeNum = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "TrdNo", i));
                tradeHistoryDto.stockCode = myXAQueryClass.GetFieldData(outBlock_occur, "IsuNo", i);
                if (tradeHistoryDto.stockCode.Length != 0)
                    tradeHistoryDto.stockCode = myXAQueryClass.GetFieldData(outBlock_occur, "IsuNo", i).Remove(0, 1);

                tradeHistoryDto.tradeType = myXAQueryClass.GetFieldData(outBlock_occur, "TpCodeNm", i);
                tradeHistoryDto.unitPrice = (int)double.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "BnsBasePrc", i));    //매매기준가.double형이지만 int로..
                tradeHistoryDto.amount = int.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "TrdQty", i));
                tradeHistoryDto.totalCost = (int)double.Parse(myXAQueryClass.GetFieldData(outBlock_occur, "FcurrAdjstAmt", i));    //정산금액, double형이지만 int로..

                StockTradeHistoryDtoList.Add(tradeHistoryDto);
                //DTO의 나머지 데이터는 주식 현재가조회등에서 채워넣는다.                
            }
            DBService.insertTransaction("insertStockTradeHistory", StockTradeHistoryDtoList);
            isRunning = false;
        }

        private void ReceiveData_FOCCQ33600(XAQueryClass myXAQueryClass)
        {
            Console.WriteLine("주식계좌 기간별 수익률 상세조회 Start");

            List<StockAccountHistoryDto> stockAccountHistoryDtoList = new List<StockAccountHistoryDto>();
            string outBlock_occur = "FOCCQ33600OutBlock3";
            int cnt = myXAQueryClass.GetBlockCount(outBlock_occur);
            for (int i = 0; i < cnt; i++)
            {
                StockAccountHistoryDto stockAccountHistoryDto = new StockAccountHistoryDto();
                stockAccountHistoryDto.accountNum = InputDataTable["AcntNo"];
                stockAccountHistoryDto.ymdDay = myXAQueryClass.GetFieldData(outBlock_occur, "BaseDt", i);
                stockAccountHistoryDto.beginingNetAssets = myXAQueryClass.GetFieldData(outBlock_occur, "FdEvalAmt", i);
                stockAccountHistoryDto.endingNetAssets = myXAQueryClass.GetFieldData(outBlock_occur, "EotEvalAmt", i);
                stockAccountHistoryDto.investedAverageBalance = myXAQueryClass.GetFieldData(outBlock_occur, "InvstAvrbalPramt", i);
                stockAccountHistoryDto.depositAmt = myXAQueryClass.GetFieldData(outBlock_occur, "MnyinSecinAmt", i);
                stockAccountHistoryDto.withdrawalAmt = myXAQueryClass.GetFieldData(outBlock_occur, "MnyoutSecoutAmt", i);
                stockAccountHistoryDto.returnOnInvestment = myXAQueryClass.GetFieldData(outBlock_occur, "EvalPnlAmt", i);

                stockAccountHistoryDtoList.Add(stockAccountHistoryDto);
            }
            DBService.insertTransaction("insertStockAccountHistory", stockAccountHistoryDtoList);
            isRunning = false;

            Console.WriteLine("주식계좌 기간별 수익률 상세조회 End");
        }



        /*
        static void ReceiveData_CDPCQ04700(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("CDPCQ04700OutBlock3");
            String[] CDPCQ04700OutBlock3Array = { "AcntNo", "TrdDt", "TrdNo", "TpCodeNm", "SmryNo", "SmryNm", "CancTpNm", "TrdQty", "Trtax", "FcurrAdjstAmt", "AdjstAmt", "OvdSum", "DpsBfbalAmt", "SellPldgRfundAmt", "DpspdgLoanBfbalAmt", "TrdmdaNm", "OrgTrdNo", "IsuNm", "TrdUprc", "CmsnAmt", "FcurrCmsnAmt", "RfundDiffAmt", "RepayAmtSum", "SecCrbalQty", "CslLoanRfundIntrstAmt", "DpspdgLoanCrbalAmt", "TrxTime", "Inouno", "IsuNo", "TrdAmt", "ChckAmt", "TaxSumAmt", "FcurrTaxSumAmt", "IntrstUtlfee", "MnyDvdAmt", "RcvblOcrAmt", "TrxBrnNo", "TrxBrnNm", "DpspdgLoanAmt", "DpspdgLoanRfundAmt", "BasePrc", "DpsCrbalAmt", "BoaAmt", "MnyoutAbleAmt", "BcrLoanOcrAmt", "BcrLoanBfbalAmt", "BnsBasePrc", "TaxchrBasePrc", "TrdUnit", "BalUnit", "EvrTax", "EvalAmt", "BcrLoanRfundAmt", "BcrLoanCrbalAmt", "AddMgnOcrTotamt", "AddMnyMgnOcrAmt", "AddMgnDfryTotamt", "AddMnyMgnDfryAmt", "BnsplAmt", "ctax", "Ihtax", "LoanDt", "CrcyCode", "FcurrAmt", "FcurrTrdAmt", "FcurrDps", "FcurrDpsBfbalAmt", "OppAcntNm", "OppAcntNo", "LoanRfundAmt", "LoanIntrstAmt", "AskpsnNm", "OrdDt", "TrdXchrat", "RdctCmsn", "FcurrStmpTx", "FcurrElecfnTrtax", "FcstckTrtax" };
            String[] CDPCQ04700OutBlock3TypeArray = { "String", "String", "double", "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "double", "double", "String", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "String", "double", "String", "double", "double", "double", "double", "double", "double", "double", "String", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "String", "double", "double", "double", "double", "String", "String", "double", "double", "String", "String", "double", "double", "double", "double", "double" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                CDPCQ04700VO cCDPCQ04700VO = new CDPCQ04700VO();
                for (int j = 0; j < CDPCQ04700OutBlock3Array.Length; j++)
                {
                    Type type = typeof(CSPAQ02200VO);
                    MethodInfo method = type.GetMethod("set" + CDPCQ04700OutBlock3Array[j]);
                    if (CDPCQ04700OutBlock3TypeArray[j] == "double")
                    {
                        method.Invoke(cCDPCQ04700VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("CDPCQ04700OutBlock3", CDPCQ04700OutBlock3Array[j], i)) });
                    }
                    else
                    {
                        method.Invoke(cCDPCQ04700VO, new object[] { myXAQueryClass.GetFieldData("CDPCQ04700OutBlock3", CDPCQ04700OutBlock3Array[j], i) });
                    }
                    al.Add(cCDPCQ04700VO);
                    //outStr += myXAQueryClass.GetFieldData("CDPCQ04700OutBlock3", CSPAQ02200OutBlock2Array[j], i) + ";";
                }
                //Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("cCDPCQ04700VOList");
            parameters.Add("cCDPCQ04700VOList", al);
            isRunning = false;
        }
        // CSPAQ02200 콜백
        static void ReceiveData_CSPAQ02200(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("CSPAQ02200OutBlock2");
            String[] CSPAQ02200OutBlock2Array = { "RecCnt", "BrnNm", "AcntNm", "MnyOrdAbleAmt", "MnyoutAbleAmt", "SeOrdAbleAmt", "KdqOrdAbleAmt", "BalEvalAmt", "RcvblAmt", "DpsastTotamt", "PnlRat", "InvstOrgAmt", "InvstPlAmt", "CrdtPldgOrdAmt", "Dps", "SubstAmt", "D1Dps", "D2Dps", "MnyrclAmt", "MgnMny", "MgnSubst", "ChckAmt", "SubstOrdAbleAmt", "MgnRat100pctOrdAbleAmt", "MgnRat35ordAbleAmt", "MgnRat50ordAbleAmt", "PrdaySellAdjstAmt", "PrdayBuyAdjstAmt", "CrdaySellAdjstAmt", "CrdayBuyAdjstAmt", "D1ovdRepayRqrdAmt", "D2ovdRepayRqrdAmt", "D1PrsmptWthdwAbleAmt", "D2PrsmptWthdwAbleAmt", "DpspdgLoanAmt", "Imreq", "MloanAmt", "ChgAfPldgRat", "OrgPldgAmt", "SubPldgAmt", "RqrdPldgAmt", "OrgPdlckAmt", "PdlckAmt", "AddPldgMny", "D1OrdAbleAmt", "CrdtIntdltAmt", "EtclndAmt", "NtdayPrsmptCvrgAmt", "OrgPldgSumAmt", "CrdtOrdAbleAmt", "SubPldgSumAmt", "CrdtPldgAmtMny", "CrdtPldgSubstAmt", "AddCrdtPldgMny", "CrdtPldgRuseAmt", "AddCrdtPldgSubst", "CslLoanAmtdt1", "DpslRestrcAmt" };
            String[] CSPAQ02200OutBlock2TypeArray = { "double", "String", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                CSPAQ02200VO cCSPAQ02200VO = new CSPAQ02200VO();
                for (int j = 0; j < CSPAQ02200OutBlock2Array.Length; j++)
                {
                    Type type = typeof(CSPAQ02200VO);
                    MethodInfo method = type.GetMethod("set" + CSPAQ02200OutBlock2Array[j]);
                    if (CSPAQ02200OutBlock2TypeArray[j] == "double")
                    {
                        method.Invoke(cCSPAQ02200VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("CSPAQ02200OutBlock2", CSPAQ02200OutBlock2Array[j], i)) });
                    }
                    else
                    {
                        method.Invoke(cCSPAQ02200VO, new object[] { myXAQueryClass.GetFieldData("CSPAQ02200OutBlock2", CSPAQ02200OutBlock2Array[j], i) });
                    }
                    al.Add(cCSPAQ02200VO);
                    outStr += myXAQueryClass.GetFieldData("CSPAQ02200OutBlock2", CSPAQ02200OutBlock2Array[j], i) + ";";
                }
                //Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("cCSPAQ02200VOList");
            parameters.Add("cCSPAQ02200VOList", al);
            isRunning = false;
        }
        // CSPAQ02300 콜백
        static void ReceiveData_CSPAQ02300(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("CSPAQ02300OutBlock3");
            String[] CSPAQ02300OutBlock3Array = { "IsuNo", "IsuNm", "SecBalPtnCode", "SecBalPtnNm", "BalQty", "BnsBaseBalQty", "CrdayBuyExecQty", "CrdaySellExecQty", "SellPrc", "BuyPrc", "SellPnlAmt", "PnlRat", "NowPrc", "CrdtAmt", "DueDt", "PrdaySellExecPrc", "PrdaySellQty", "PrdayBuyExecPrc", "PrdayBuyQty", "LoanDt", "AvrUprc", "SellAbleQty", "SellOrdQty", "CrdayBuyExecAmt", "CrdaySellExecAmt", "PrdayBuyExecAmt", "PrdaySellExecAmt", "BalEvalAmt", "EvalPnl", "MnyOrdAbleAmt", "OrdAbleAmt", "SellUnercQty", "SellUnsttQty", "BuyUnercQty", "BuyUnsttQty", "UnsttQty", "UnercQty", "PrdayCprc", "PchsAmt", "RegMktCode", "LoanDtlClssCode", "DpspdgLoanQty" };
            String[] CSPAQ02300OutBlock3TypeArray = { "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "double", "double", "double", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "String", "double" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                CSPAQ02300VO cCSPAQ02300VO = new CSPAQ02300VO();
                for (int j = 0; j < CSPAQ02300OutBlock3Array.Length; j++)
                {
                    Type type = typeof(CSPAQ02300VO);
                    MethodInfo method = type.GetMethod("set" + CSPAQ02300OutBlock3Array[j]);
                    if (CSPAQ02300OutBlock3TypeArray[j] == "double")
                    {
                        method.Invoke(cCSPAQ02300VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("CSPAQ02300OutBlock3", CSPAQ02300OutBlock3Array[j], i)) });
                    }
                    else
                    {
                        method.Invoke(cCSPAQ02300VO, new object[] { myXAQueryClass.GetFieldData("CSPAQ02300OutBlock3", CSPAQ02300OutBlock3Array[j], i) });
                    }
                    al.Add(cCSPAQ02300VO);
                    outStr += myXAQueryClass.GetFieldData("CSPAQ02300OutBlock3", CSPAQ02300OutBlock3Array[j], i) + ";";
                }
               // Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("cCSPAQ02300VOList");
            parameters.Add("cCSPAQ02300VOList", al);
            isRunning = false;
        }
        // t0425 콜백
        static void ReceiveData_t0425(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("t0425OutBlock1");
            String[] t0425OutBlock1Array = { "ordno", "expcode", "medosu", "qty", "price", "cheqty", "cheprice", "ordrem", "cfmqty", "status", "orgordno", "ordgb", "ordtime", "ordermtd", "sysprocseq", "hogagb", "price1", "orggb", "singb", "loandt" };
            String[] t0425OutBlock1MethodArray = { "Ordno", "Expcode", "Medosu", "Qty", "Price", "Cheqty", "Cheprice", "Ordrem", "Cfmqty", "Status", "Orgordno", "Ordgb", "Ordtime", "Ordermtd", "Sysprocseq", "Hogagb", "Price1", "Orggb", "Singb", "Loandt" };
            String[] t0425OutBlock1TypeArray = { "double", "String", "String", "double", "double", "double", "double", "double", "double", "String", "double", "String", "String", "String", "double", "String", "double", "String", "String", "String" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                t0425VO ct0425VO = new t0425VO();
                for (int j = 0; j < t0425OutBlock1Array.Length; j++)
                {
                    Type type = typeof(t0425VO);
                    MethodInfo method = type.GetMethod("set" + t0425OutBlock1MethodArray[j]);
                    if (t0425OutBlock1TypeArray[j] == "double")
                    {
                        method.Invoke(ct0425VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("t0425OutBlock1", t0425OutBlock1Array[j], i)) });
                    }
                    else
                    {
                        method.Invoke(ct0425VO, new object[] { myXAQueryClass.GetFieldData("t0425OutBlock1", t0425OutBlock1Array[j], i) });
                    }
                    al.Add(ct0425VO);
                    outStr += myXAQueryClass.GetFieldData("t0425OutBlock1", t0425OutBlock1Array[j], i) + ";";
                }
                //Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("ct0425VOList");
            parameters.Add("ct0425VOList", al);
            isRunning = false;
        }
        // t1102 콜백
        static void ReceiveData_t1102(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("t1102OutBlock");
            String[] t1102OutBlockArray = { "hname", "price", "sign", "change", "diff", "volume", "recprice", "avg", "uplmtprice", "dnlmtprice", "jnilvolume", "volumediff", "open", "opentime", "high", "hightime", "low", "lowtime", "high52w", "high52wdate", "low52w", "low52wdate", "exhratio", "per", "pbrx", "listing", "jkrate", "memedan", "offernocd1", "bidnocd1", "offerno1", "bidno1", "dvol1", "svol1", "dcha1", "scha1", "ddiff1", "sdiff1", "offernocd2", "bidnocd2", "offerno2", "bidno2", "dvol2", "svol2", "dcha2", "scha2", "ddiff2", "sdiff2", "offernocd3", "bidnocd3", "offerno3", "bidno3", "dvol3", "svol3", "dcha3", "scha3", "ddiff3", "sdiff3", "offernocd4", "bidnocd4", "offerno4", "bidno4", "dvol4", "svol4", "dcha4", "scha4", "ddiff4", "sdiff4", "offernocd5", "bidnocd5", "offerno5", "bidno5", "dvol5", "svol5", "dcha5", "scha5", "ddiff5", "sdiff5", "fwdvl", "ftradmdcha", "ftradmddiff", "fwsvl", "ftradmscha", "ftradmsdiff", "vol", "shcode", "value", "jvolume", "highyear", "highyeardate", "lowyear", "lowyeardate", "target", "capital", "abscnt", "parprice", "gsmm", "subprice", "total", "listdate", "name", "bfsales", "bfoperatingincome", "bfordinaryincome", "bfnetincome", "bfeps", "name2", "bfsales2", "bfoperatingincome2", "bfordinaryincome2", "bfnetincome2", "bfeps2", "salert", "opert", "ordrt", "netrt", "epsrt", "info1", "info2", "info3", "info4", "janginfo", "t_per", "tonghwa", "dval1", "sval1", "dval2", "sval2", "dval3", "sval3", "dval4", "sval4", "dval5", "sval5", "davg1", "savg1", "davg2", "savg2", "davg3", "savg3", "davg4", "savg4", "davg5", "savg5", "ftradmdval", "ftradmsval", "ftradmdvag", "ftradmsvag", "info5", "spac_gubun", "issueprice", "alloc_gubun", "alloc_text" };
            String[] t1102OutBlockMethodArray = { "Hname", "Price", "Sign", "Change", "Diff", "Volume", "Recprice", "Avg", "Uplmtprice", "Dnlmtprice", "Jnilvolume", "Volumediff", "Open", "Opentime", "High", "Hightime", "Low", "Lowtime", "High52w", "High52wdate", "Low52w", "Low52wdate", "Exhratio", "Per", "Pbrx", "Listing", "Jkrate", "Memedan", "Offernocd1", "Bidnocd1", "Offerno1", "Bidno1", "Dvol1", "Svol1", "Dcha1", "Scha1", "Ddiff1", "Sdiff1", "Offernocd2", "Bidnocd2", "Offerno2", "Bidno2", "Dvol2", "Svol2", "Dcha2", "Scha2", "Ddiff2", "Sdiff2", "Offernocd3", "Bidnocd3", "Offerno3", "Bidno3", "Dvol3", "Svol3", "Dcha3", "Scha3", "Ddiff3", "Sdiff3", "Offernocd4", "Bidnocd4", "Offerno4", "Bidno4", "Dvol4", "Svol4", "Dcha4", "Scha4", "Ddiff4", "Sdiff4", "Offernocd5", "Bidnocd5", "Offerno5", "Bidno5", "Dvol5", "Svol5", "Dcha5", "Scha5", "Ddiff5", "Sdiff5", "Fwdvl", "Ftradmdcha", "Ftradmddiff", "Fwsvl", "Ftradmscha", "Ftradmsdiff", "Vol", "Shcode", "Value", "Jvolume", "Highyear", "Highyeardate", "Lowyear", "Lowyeardate", "Target", "Capital", "Abscnt", "Parprice", "Gsmm", "Subprice", "Total", "Listdate", "Name", "Bfsales", "Bfoperatingincome", "Bfordinaryincome", "Bfnetincome", "Bfeps", "Name2", "Bfsales2", "Bfoperatingincome2", "Bfordinaryincome2", "Bfnetincome2", "Bfeps2", "Salert", "Opert", "Ordrt", "Netrt", "Epsrt", "Info1", "Info2", "Info3", "Info4", "Janginfo", "T_per", "Tonghwa", "Dval1", "Sval1", "Dval2", "Sval2", "Dval3", "Sval3", "Dval4", "Sval4", "Dval5", "Sval5", "Davg1", "Savg1", "Davg2", "Savg2", "Davg3", "Savg3", "Davg4", "Savg4", "Davg5", "Savg5", "Ftradmdval", "Ftradmsval", "Ftradmdvag", "Ftradmsvag", "Info5", "Spac_gubun", "Issueprice", "Alloc_gubun", "Alloc_text" };
            String[] t1102OutBlockTypeArray = { "String", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "double", "String", "double", "String", "double", "String", "double", "String", "double", "double", "double", "double", "double", "String", "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "String", "String", "String", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "double", "double", "double", "String", "double", "String", "double", "double", "double", "double", "String", "double", "double", "String", "String", "double", "double", "double", "double", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "String", "String", "String", "String", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "String", "double", "String", "String" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                t1102VO ct1102VO = new t1102VO();
                for (int j = 0; j < t1102OutBlockArray.Length; j++)
                {
                    Type type = typeof(t1102VO);
                    MethodInfo method = type.GetMethod("set" + t1102OutBlockMethodArray[j]);
                    if (t1102OutBlockTypeArray[j] == "double")
                    {
                        if (myXAQueryClass.GetFieldData("t1102OutBlock", t1102OutBlockArray[j], i) == null || myXAQueryClass.GetFieldData("t1102OutBlock", t1102OutBlockArray[j], i) == "")
                        {
                            method.Invoke(ct1102VO, new object[] { 0.0 });
                        }
                        else
                        {
                            method.Invoke(ct1102VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("t1102OutBlock", t1102OutBlockArray[j], i)) });
                        }
                    }
                    else
                    {
                        method.Invoke(ct1102VO, new object[] { myXAQueryClass.GetFieldData("t1102OutBlock", t1102OutBlockArray[j], i) });
                    }
                    al.Add(ct1102VO);
                    outStr += myXAQueryClass.GetFieldData("t1102OutBlock", t1102OutBlockArray[j], i) + ";";
                }
               // Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("ct1102VOList");
            parameters.Add("ct1102VOList", al);
            isRunning = false;
        }
        // t2105 콜백
        static void ReceiveData_t2105(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("t2105OutBlock");
            String[] t2105OutBlockArray = { "hname", "price", "sign", "change", "diff", "volume", "stimeqrt", "jnilclose", "offerho1", "bidho1", "offerrem1", "bidrem1", "dcnt1", "scnt1", "offerho2", "bidho2", "offerrem2", "bidrem2", "dcnt2", "scnt2", "offerho3", "bidho3", "offerrem3", "bidrem3", "dcnt3", "scnt3", "offerho4", "bidho4", "offerrem4", "bidrem4", "dcnt4", "scnt4", "offerho5", "bidho5", "offerrem5", "bidrem5", "dcnt5", "scnt5", "dvol", "svol", "toffernum", "tbidnum", "time", "shcode" };
            String[] t2105OutBlockMethodArray = { "Hname", "Price", "Sign", "Change", "Diff", "Volume", "Stimeqrt", "Jnilclose", "Offerho1", "Bidho1", "Offerrem1", "Bidrem1", "Dcnt1", "Scnt1", "Offerho2", "Bidho2", "Offerrem2", "Bidrem2", "Dcnt2", "Scnt2", "Offerho3", "Bidho3", "Offerrem3", "Bidrem3", "Dcnt3", "Scnt3", "Offerho4", "Bidho4", "Offerrem4", "Bidrem4", "Dcnt4", "Scnt4", "Offerho5", "Bidho5", "Offerrem5", "Bidrem5", "Dcnt5", "Scnt5", "Dvol", "Svol", "Toffernum", "Tbidnum", "Time", "Shcode" };
            String[] t2105OutBlockTypeArray = { "String", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "String" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                t2105VO ct2105VO = new t2105VO();
                for (int j = 0; j < t2105OutBlockArray.Length; j++)
                {
                    Type type = typeof(t2105VO);
                    MethodInfo method = type.GetMethod("set" + t2105OutBlockMethodArray[j]);
                    if (t2105OutBlockTypeArray[j] == "double")
                    {
                        method.Invoke(ct2105VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("t2105OutBlock", t2105OutBlockArray[j], i)) });
                    }
                    else
                    {
                        method.Invoke(ct2105VO, new object[] { myXAQueryClass.GetFieldData("t2105OutBlock", t2105OutBlockArray[j], i) });
                    }
                    al.Add(ct2105VO);
                    outStr += myXAQueryClass.GetFieldData("t2105OutBlock", t2105OutBlockArray[j], i) + ";";
                }
                //Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("ct2105VOList");
            parameters.Add("ct2105VOList", al);
            isRunning = false;
        }
        // CSPAT00600 콜백
        static void ReceiveData_CSPAT00600(XAQueryClass myXAQueryClass)
        {
            
            isRunning = false;
        }
        // t1511 콜백
        static void ReceiveData_t1511(XAQueryClass myXAQueryClass)
        {
            int blockCount = myXAQueryClass.GetBlockCount("t1511OutBlock");
            String[] t1511OutBlockArray = { "gubun", "hname", "pricejisu", "jniljisu", "sign", "change", "diffjisu", "jnilvolume", "volume", "volumechange", "volumerate", "jnilvalue", "value", "valuechange", "valuerate", "openjisu", "opendiff", "opentime", "highjisu", "highdiff", "hightime", "lowjisu", "lowdiff", "lowtime", "whjisu", "whchange", "whjday", "wljisu", "wlchange", "wljday", "yhjisu", "yhchange", "yhjday", "yljisu", "ylchange", "yljday", "firstjcode", "firstjname", "firstjisu", "firsign", "firchange", "firdiff", "secondjcode", "secondjname", "secondjisu", "secsign", "secchange", "secdiff", "thirdjcode", "thirdjname", "thirdjisu", "thrsign", "thrchange", "thrdiff", "fourthjcode", "fourthjname", "fourthjisu", "forsign", "forchange", "fordiff", "highjo", "upjo", "unchgjo", "lowjo", "downjo" };
            String[] t1511OutBlockMethodArray = { "Gubun", "Hname", "Pricejisu", "Jniljisu", "Sign", "Change", "Diffjisu", "Jnilvolume", "Volume", "Volumechange", "Volumerate", "Jnilvalue", "Value", "Valuechange", "Valuerate", "Openjisu", "Opendiff", "Opentime", "Highjisu", "Highdiff", "Hightime", "Lowjisu", "Lowdiff", "Lowtime", "Whjisu", "Whchange", "Whjday", "Wljisu", "Wlchange", "Wljday", "Yhjisu", "Yhchange", "Yhjday", "Yljisu", "Ylchange", "Yljday", "Firstjcode", "Firstjname", "Firstjisu", "Firsign", "Firchange", "Firdiff", "Secondjcode", "Secondjname", "Secondjisu", "Secsign", "Secchange", "Secdiff", "Thirdjcode", "Thirdjname", "Thirdjisu", "Thrsign", "Thrchange", "Thrdiff", "Fourthjcode", "Fourthjname", "Fourthjisu", "Forsign", "Forchange", "Fordiff", "Highjo", "Upjo", "Unchgjo", "Lowjo", "Downjo" };
            String[] t1511OutBlockTypeArray = { "String", "String", "double", "double", "String", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "String", "double", "double", "String", "double", "double", "String", "double", "double", "String", "double", "double", "String", "double", "double", "String", "double", "double", "String", "String", "String", "double", "String", "double", "double", "String", "String", "double", "String", "double", "double", "String", "String", "double", "String", "double", "double", "String", "String", "double", "String", "double", "double", "double", "double", "double", "double", "double" };
            ArrayList al = new ArrayList();
            for (int i = 0; i < blockCount; i++)
            {
                String outStr = "";
                t1511VO ct1511VO = new t1511VO();
                for (int j = 0; j < t1511OutBlockArray.Length; j++)
                {
                    Type type = typeof(t1511VO);
                    MethodInfo method = type.GetMethod("set" + t1511OutBlockMethodArray[j]);
                    if (t1511OutBlockTypeArray[j] == "double")
                    {
                        method.Invoke(ct1511VO, new object[] { double.Parse(myXAQueryClass.GetFieldData("t1511OutBlock", t1511OutBlockArray[j], i)) });
                    }
                    else
                    {
                        method.Invoke(ct1511VO, new object[] { myXAQueryClass.GetFieldData("t1511OutBlock", t1511OutBlockArray[j], i) });
                    }
                    al.Add(ct1511VO);
                    outStr += myXAQueryClass.GetFieldData("t1511OutBlock", t1511OutBlockArray[j], i) + ";";
                }
               // Debug.Out.WriteLine(outStr);
            }
            parameters.Remove("ct1511VOList");
            parameters.Add("ct1511VOList", al);
            isRunning = false;
        }
        */
        public void kospiStockRealTime(IList<string> kospiStockCodeList)
        {
            Debug.WriteLine("kospiStockRealTime start");
            S3_ kodaqStockReal = new S3_();
            kodaqStockReal.CallBackMethod += ReceiveRealData_stockContractReal;
            foreach (string stockCode in kospiStockCodeList)
            {
                Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                InputDataTable.Add("shcode", stockCode);
                kodaqStockReal.AdviseExcute(InputDataTable);
                Debug.WriteLine("stock code :" + stockCode + " advised");
            }
        }
        public void kosdaqStockRealTime(IList<string> kodaqStockCodeList)
        {
            Debug.WriteLine("kosdaqStockRealTime start");
            K3_ kodaqStockReal = new K3_();
            kodaqStockReal.CallBackMethod += ReceiveRealData_stockContractReal;
            foreach (string stockCode in kodaqStockCodeList)
            {
                Dictionary<string, string> InputDataTable = new Dictionary<string, string>();
                InputDataTable.Add("shcode", stockCode);
                kodaqStockReal.AdviseExcute(InputDataTable);
            }
        }

        private void ReceiveRealData_myStockReal(XARealClass myXARealClass)
        {
            Debug.WriteLine("MY REAL STOCK PROCESSING...");
            string stockCode = myXARealClass.GetFieldData("OutBlock", "shcode");
            string time = myXARealClass.GetFieldData("OutBlock", "chetime");
            double fluctuation = double.Parse(myXARealClass.GetFieldData("OutBlock", "drate"));
            string price = myXARealClass.GetFieldData("OutBlock", "price");
            double cpower = double.Parse(myXARealClass.GetFieldData("OutBlock", "cpower"));
            long yesterdayVolume = long.Parse(myXARealClass.GetFieldData("OutBlock", "jnilvolume"));
            long volume = long.Parse(myXARealClass.GetFieldData("OutBlock", "volume"));


            //Task<long> result = RedisFactory.getInstance.Lists.GetLength(0, MessageCode.TGRAM_MSG_QUEUE);
            Task<byte[]> storedPrice = RedisFactory.getInstance.Strings.Get(0, stockCode + "_price");
            if ( string.IsNullOrEmpty(Encoding.UTF8.GetString(storedPrice.Result))) 
            {
                RedisFactory.getInstance.Strings.Set(0, stockCode + "_price", price);
                RedisFactory.getInstance.Strings.Set(0, stockCode + "_price", price); // TODO 종목이름 가져오기. --> 이것은 초반에 시작할때 가져오는게 좋을듯.
            }
            else 
            {
                double curPrice = double.Parse((Encoding.UTF8.GetString(storedPrice.Result)));

                if ( Math.Abs( ( curPrice - double.Parse(price) ) / 100 ) >= ComnConstants.FLUCTUATION_TRESHOLD )
                {
                    RedisFactory.getInstance.Strings.Set(0, stockCode + "_price", price);
                    RedisFactory.getInstance.Lists.AddLast(0, MessageCode.TGRAM_MSG_QUEUE, Encoding.UTF8.GetBytes(stockCode + "\n" + MessageCode.TGRAM_OVER_VARIABILITY));
                }

            }
            RedisFactory.getInstance.Strings.Set(0, stockCode, Encoding.UTF8.GetBytes("뭐야이거"));



        }
        private void ReceiveRealData_stockContractReal(XARealClass myXARealClass)
        {
            Console.WriteLine("REAL DATA PROCESSING...");
            Debug.WriteLine("REAL DATA PROCESSING...");
            string stockCode = myXARealClass.GetFieldData("OutBlock", "shcode");
            string time = myXARealClass.GetFieldData("OutBlock", "chetime");
            double fluctuation = double.Parse(myXARealClass.GetFieldData("OutBlock", "drate"));
            long price = long.Parse(myXARealClass.GetFieldData("OutBlock", "price"));
            double cpower = double.Parse(myXARealClass.GetFieldData("OutBlock", "cpower"));
            long yesterdayVolume = long.Parse(myXARealClass.GetFieldData("OutBlock", "jnilvolume"));
            long volume = long.Parse(myXARealClass.GetFieldData("OutBlock", "volume"));


            StockSignalDto stockSignalDto = new StockSignalDto();
            stockSignalDto.stockCode = stockCode;
            stockSignalDto.contractTime = time;
            stockSignalDto.curPrice = price;
            stockSignalDto.fluctuation = fluctuation;
            stockSignalDto.contractStrength = cpower;
            stockSignalDto.curTimeVolume = volume;
            stockSignalDto.beforeTimeVolume = yesterdayVolume;
            stockSignalDto.signalName = "CONT_STR_UP";
            stockSignalDto.moreInfo = "";


            DaoFactory.getInstance.Insert("insertStockSignal", stockSignalDto);

            if (volume > yesterdayVolume * 4
                && cpower > 100
                && price > 2000
                && fluctuation < 5
                && int.Parse(time) > 090500 && int.Parse(time) < 130000)
            {
                //StockSignalDto stockSignalDto = new StockSignalDto(stockCode, time, price, fluctuation, cpower, volume, yesterdayVolume, "CONT_STR_UP" ,"");
                IDictionary<string, string> inputDic = new Dictionary<string, string>();
                inputDic.Add("stockCode", stockCode);
                inputDic.Add("contractTime", time);
                inputDic.Add("maxLimit", "30");

                IList<StockSignalDto> stockContractList_30Tick = DaoFactory.getInstance.QueryForList<StockSignalDto>("selectTickStockSignal", inputDic);


                /*
                 * 이 전략과 10분이 딱 됐을때 ORDER BY 해서 가장 큰거 세개를 고르는 전략 비교..해보자
                 */
                if (stockContractList_30Tick.Count > 10)
                {
                    if (stockContractList_30Tick.ElementAt(1).contractStrength - cpower > 10
                    && stockContractList_30Tick.ElementAt(5).contractStrength - cpower > 20
                    && stockContractList_30Tick.ElementAt(10).contractStrength - cpower > 30)
                    {
                        //XingFactory.getInstance.buyStock(stockCode, price.ToString(), "10");

                        addBuyStockCodeList(stockSignalDto, "gradually increase");
                    }

                    /*체결강도 변화량은 현재 체결강도 크기가 작으면 커야하는 반비례 관계이므로
                     * k / 150 = 10  ( k : 체결강도 변화량 관련 계수, 150 은 기준 현재 체결량 , 10은 체결강도 변화량
                     * 일억 초과 매수를 함으로써.. 강도가 확뛰었을때
                     */
                    else if (1500 / cpower < (cpower - stockContractList_30Tick.ElementAt(1).contractStrength)
                             && price * (volume - stockContractList_30Tick.ElementAt(1).curTimeVolume) > 100000000)
                    {
                        //XingFactory.getInstance.buyStock(stockCode, price.ToString(), "10");

                        addBuyStockCodeList(stockSignalDto, "suddenly increase");
                    }
                }

            }
        }

        private void addBuyStockCodeList(StockSignalDto stockSignalDto, string message)
        {
            if (sharedObject["buyStockCodeList"] == null)
                sharedObject["buyStockCodeDic"] = new Dictionary<string, StockSignalDto>();

            stockSignalDto.moreInfo = "BUY";

            if (!((Dictionary<string, StockSignalDto>)sharedObject["buyStockCodeList"]).ContainsKey(stockSignalDto.stockCode))
            {
                ((Dictionary<string, StockSignalDto>)sharedObject["buyStockCodeDic"]).Add(stockSignalDto.stockCode, stockSignalDto);

                DaoFactory.getInstance.Update("updateStockSignalMoreInfo", stockSignalDto);
                MyPeopleMessage.sendMsg(message + " buy : " + stockSignalDto.ToString());
            }
        }




    }
}
