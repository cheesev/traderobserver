﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    // 계좌거래 내역 조회
    class FOCCQ33600 : Query
    {
        public FOCCQ33600() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\FOCCQ33600.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        public FOCCQ33600(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\FOCCQ33600.res";
            GetInBlockName();
        }

        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            base.QueryExcute(InputDataTable);
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            base.myXAQueryClass_ReceiveData(szTrCode);
        }

    }
}
