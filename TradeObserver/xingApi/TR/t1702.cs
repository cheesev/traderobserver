﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    class t1702 : Query
    {
        public t1702() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t1702.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">
        /// daygb(당일 :0 , 전일 : 1),
        /// timegb(분 : 0 틱 : 1)
        /// shcode
        /// endtime
        /// cts_time
        /// </param>
        public t1702(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t1702.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            base.QueryExcute(InputDataTable);
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            string cts_idx = myXAQueryClass.GetFieldData("t1702OutBlock", "cts_idx", 0);
            string cts_date = myXAQueryClass.GetFieldData("t1702OutBlock", "cts_date", 0);

            base.myXAQueryClass_ReceiveData(szTrCode);
            if (myXAQueryClass.IsNext && cts_date != "" && QueryExecutor.isRunning)
            {
                Thread.Sleep(500);
                myXAQueryClass.SetFieldData("t1702InBlock", "cts_date", 0, cts_date);
                myXAQueryClass.SetFieldData("t1702InBlock", "cts_idx", 0, cts_idx);
                myXAQueryClass.Request(true);
            }
            else
            {
                Thread.Sleep(500);
                QueryExecutor.isRunning = false;
            }
        }
    }
}
