﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    // 현물 정상 주문
    class CSPAT00600 : Query
    {
        public CSPAT00600() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\CSPAT00600.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">
        /// </param>
        public CSPAT00600(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\CSPAT00600.res";
            GetInBlockName();
        }

        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            base.QueryExcute(InputDataTable);
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            base.myXAQueryClass_ReceiveData(szTrCode);
        }
    }
}
