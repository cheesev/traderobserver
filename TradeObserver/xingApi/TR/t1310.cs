﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    class t1310 : Query
    {
        public t1310() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t1310.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">
        /// daygb(당일 :0 , 전일 : 1),
        /// timegb(분 : 0 틱 : 1)
        /// shcode
        /// endtime
        /// cts_time
        /// </param>
        public t1310(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t1310.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            Console.WriteLine("query execute");
            base.QueryExcute(InputDataTable);
            Console.WriteLine("query execute end");
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            Console.WriteLine("receive call");
            string cts_time = myXAQueryClass.GetFieldData("t1310OutBlock", "cts_time", 0);

            base.myXAQueryClass_ReceiveData(szTrCode);
            if (myXAQueryClass.IsNext)
            {
                Thread.Sleep(100);
                myXAQueryClass.SetFieldData("t1310InBlock", "cts_time", 0, cts_time);
                myXAQueryClass.Request(true);
            }
            else
            {
                Thread.Sleep(500);
                QueryExecutor.isRunning = false;
            }
            Console.WriteLine("receive call end ");
        }
    }
}
