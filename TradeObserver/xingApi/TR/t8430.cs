﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    class t8430 : Query
    {
        public t8430() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t8430.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        public t8430(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t8430.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            base.QueryExcute(InputDataTable);
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            base.myXAQueryClass_ReceiveData(szTrCode);
        }

    }
}
