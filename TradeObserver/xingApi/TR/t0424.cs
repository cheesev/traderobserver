﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    class t0424 : Query
    {
        public t0424() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t0424.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">
        /// accno(계좌번호),
        /// passwd(비밀번호)
        /// 
        /// </param>
        public t0424(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\t0424.res";
            GetInBlockName();
        }


        /// <param name="InputDataTable">focode(단축코드)</param>
        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            base.QueryExcute(InputDataTable);
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            base.myXAQueryClass_ReceiveData(szTrCode);
        }
    }
}
