﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.TR
{
    // 계좌거래 내역 조회
    class CDPCQ04700 : Query
    {
        public CDPCQ04700() : base()
        {
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\CDPCQ04700.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        public CDPCQ04700(Dictionary<string, string> InputDataTable) : base()
        {
            this.InputDataTable = InputDataTable;
            myXAQueryClass.ResFileName = @"C:\ETRADE\xingAPI\Res\CDPCQ04700.res";
            GetInBlockName();
        }

        /// <param name="InputDataTable">focode(단축코드)</param>
        /// RecCnt : "1"
        /// QryTp : 조회구분
        /// AcntNo : 계좌번호 
        /// Pwd : 비밀번호
        /// QrySrtDt : 조회시작일
        /// QryEndDt : 조회종료일
        /// ....and so on ( 더 있지만 잘 안쓸거같음)
        public override void QueryExcute(Dictionary<string, string> InputDataTable)
        {
            base.QueryExcute(InputDataTable);
        }

        protected override void myXAQueryClass_ReceiveData(string szTrCode)
        {
            base.myXAQueryClass_ReceiveData(szTrCode);
        }

    }
}
