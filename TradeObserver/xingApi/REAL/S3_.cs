﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.xingApi.REAL
{
    class S3_ : Real
    {
        public S3_() : base()
        {
            myXARealClass.ResFileName = @"C:\ETRADE\xingAPI\Res\S3_.res";
        }

        /// <param name="InputDataTable">shcode : 단축코드</param>
        public override void AdviseExcute(Dictionary<string, string> InputDataTable)
        {
            base.AdviseExcute(InputDataTable);
        }

        protected override void myXARealClass_ReceiveRealData(string szTrCode)
        {
            base.myXARealClass_ReceiveRealData(szTrCode);
        }
    }
}
