﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.common.dto;
using TradeObserver.common.message;
using TradeObserver.common.service;
using TradeObserver.common.util;
using TradeObserver.dto.stock;

namespace TradeObserver.xingApi
{
    class XingApiCustomService
    {
        public QueryExecutor queryExecutor { get; set; }
        public CommonInfoDto commonInfoDto { get; set; }

        /* simulation 을 위한 변수 들 */
        public int tryCnt { get; set; }
        public int sucCnt { get; set; }


        public XingApiCustomService()
        {
            queryExecutor = new QueryExecutor();
            commonInfoDto = new CommonInfoDto();
            //commonInfoDto.server = "demo.etrade.co.kr";
            commonInfoDto.server = "hts.etrade.co.kr";
            UserInfoDto userInfoDto = DaoFactory.getInstance.QueryForObject<UserInfoDto>("selectUserInfo", null);

            commonInfoDto.acctInfoDtoList = DaoFactory.getInstance.QueryForList<AcctInfoDto>("selectAcctInfo", null);
            commonInfoDto.userInfoDto = userInfoDto;
        }

        public void login()
        {
            Dictionary<string, string> inputTable = new Dictionary<string, string>();
            inputTable.Add("server", commonInfoDto.server);
            inputTable.Add("loginId", commonInfoDto.userInfoDto.loginId);
            inputTable.Add("loginPwd", commonInfoDto.userInfoDto.loginPwd);
            inputTable.Add("pkiPwd", commonInfoDto.userInfoDto.pkiPwd);

            //로그인
            queryExecutor.InputDataTable = inputTable;
            queryExecutor.startTr("login");
        }

        public void logout()
        {
            queryExecutor.startTr("logout");
        }

        public void updateAllStockInfo()
        {
            try
            {
                DaoFactory.getInstance.Delete("deleteAllStockInfo", null);
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            Dictionary<string, string> inputTable = new Dictionary<string, string>();
            inputTable.Add("gubun", "0");
            queryExecutor.InputDataTable = inputTable;
            queryExecutor.startTr("t8430");
            
            IList<string> stockCodeList = DaoFactory.getInstance.QueryForList<string>("selectStockInfo", null);
            foreach (string stockCode in stockCodeList)
            {
                queryExecutor.InputDataTable.Clear();
                queryExecutor.InputDataTable["shcode"] = stockCode;
                queryExecutor.InputDataTable["dwmcode"] = "1";
                queryExecutor.InputDataTable["idx"] = "";
                queryExecutor.InputDataTable["cnt"] = "1";
                queryExecutor.InputDataTable["date"] = DateTime.Now.ToString("yyyyMMdd");
                commonInfoDto.currentOpName = "updateAllStockInfo";
                queryExecutor.startTr("t1305");
            }


        }

        public double addStockSignalList(StockSignalDto stockSignalDto, string message)
        {
            double result = 0;
            if (commonInfoDto.stockSignalList == null)
                commonInfoDto.stockSignalList = new Dictionary<string, StockSignalDto>();

            stockSignalDto.moreInfo = "BUY";

            if(!commonInfoDto.stockSignalList.ContainsKey(stockSignalDto.stockCode))
            {
                commonInfoDto.stockSignalList.Add(stockSignalDto.stockCode, stockSignalDto);
                DaoFactory.getInstance.Update("updateStockSignalMoreInfo", stockSignalDto);
                Debug.WriteLine(message + " buy : " + stockSignalDto.ToString());

                //시뮬레이션을 위해 추가.

                result = ((double)(DaoFactory.getInstance.QueryForObject<Decimal>("selectMaxFlucStockSignal", stockSignalDto)) - stockSignalDto.fluctuation);
                Debug.WriteLine(stockSignalDto.ToString() + " : max - now result : " + result);
                return result;
                //MyPeopleMessage.sendMsg(message + " buy : " + stockSignalDto.ToString());
            }
            return -100;
        }

        public void buyStock(string acctNum, string acctPwd, string stockCode, string price, string amount)
        {
            stockCode = "A" + stockCode;
            Dictionary<string, string> inputTable = new Dictionary<string, string>();
            inputTable["AcntNo"] = acctNum;
            inputTable["InptPwd"] = acctPwd;
            inputTable["IsuNo"] = stockCode;
            inputTable["OrdPrc"] = price; //주문가
            inputTable["OrdQty"] = amount; //주문수량
            inputTable["BnsTpCode"] = "2"; // 1: 매도 2 : 매수
            inputTable["OrdprcPtnCode"] = "00"; // 00: 지정가 03 : 시장가
            inputTable["MgntrnCode"] = "000"; // 000: 보통
            inputTable["OrdCndiTpCode"] = "0"; // 0 없음
            queryExecutor.InputDataTable = inputTable;
            queryExecutor.startTr("CSPAT00600");
        }
        public void sellStock(string acctNum, string acctPwd, string stockCode, string price, string amount)
        {
            stockCode = "A" + stockCode;
            Dictionary<string, string> inputTable = new Dictionary<string, string>();
            inputTable["AcntNo"] = acctNum;
            inputTable["InptPwd"] = acctPwd;
            inputTable["IsuNo"] = stockCode;
            inputTable["OrdPrc"] = price; //주문가
            inputTable["OrdQty"] = amount; //주문수량
            inputTable["BnsTpCode"] = "1"; // 1: 매도 2 : 매수
            inputTable["OrdprcPtnCode"] = "00"; // 00: 지정가 03 : 시장가
            inputTable["MgntrnCode"] = "000"; // 000: 보통
            inputTable["OrdCndiTpCode"] = "0"; // 0 없음
            queryExecutor.InputDataTable = inputTable;
            queryExecutor.startTr("CSPAT00600");
        }

        /*
         * marketDiv - 1 : KOSPI , 2 : KOSDAQ
         * 2014.05.06 - get real stock data which price same or larger than 2000
         */
        public void getRealInterestStock()
        {
            StringBuilder myStock = new StringBuilder();
            
            StockInfoDto stockInfoDto = new StockInfoDto();
            stockInfoDto.gubun = "1";
            //stockInfoDto.note = "CURRENT";
            //inputDic.Add("note2", "CURRENT");

            IList<string> stockCodeList = null;
            stockCodeList = DaoFactory.getInstance.QueryForList<string>("selectInterestStockInfo", stockInfoDto);
            foreach (string stockCode in stockCodeList) 
            {
                myStock.Append(stockCode).Append(",");
            }

            queryExecutor.InputDataTable["myKospiStock"] = myStock.ToString();
            myStock.Clear();
            // 코스피를 앞에 코스닥을 뒤에 넣고.. string 을 A,B,C,/D,E,F, 로 구분 (ABC는 코스피, DEF는 코스닥)
            //myStock.Append("/");
            stockInfoDto.gubun = "2";
            stockCodeList = DaoFactory.getInstance.QueryForList<string>("selectInterestStockInfo", stockInfoDto);
            foreach (string stockCode in stockCodeList)
            {
                myStock.Append(stockCode).Append(",");
            }
            queryExecutor.InputDataTable["myKosdaqStock"] = myStock.ToString();

            queryExecutor.startTr("MARKET_REAL");
        }

        public void insertAllStockTradeSituationByDay()
        {
            StockInfoDto stockInfoDto = new StockInfoDto();
            stockInfoDto.etfgubun = "0";

            IList<string> stockCodeList = DaoFactory.getInstance.QueryForList<string>("selectStockInfoByEtfGubun", stockInfoDto);

            foreach (string stockCode in stockCodeList)
                insertStockTradeSituationByDay(stockCode);
        }

        public void insertStockTradeSituationByDay(string stockCode)
        {
            queryExecutor.InputDataTable.Clear();
            queryExecutor.InputDataTable["shcode"] = stockCode;
            Console.WriteLine("current processing....." + stockCode);
            QueryExecutor.sharedObject["maxDate"] = DaoFactory.getInstance.QueryForObject<string>("selectMaxStockTradeSituationByDay"
                                                                                                 ,new StockTradeSituationByDayDto().stockCode = stockCode);
            if (QueryExecutor.sharedObject["maxDate"] != null
                && QueryExecutor.sharedObject["maxDate"].ToString().Equals(DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture)))
                return;
            queryExecutor.InputDataTable["volvalgb"] = "1";
            queryExecutor.InputDataTable["msmdgb"] = "0";
            queryExecutor.InputDataTable["cumulgb"] = "0";
            queryExecutor.startTr("t1702");
        }

        public void insertTradeHistory()
        {
            // 70일전거 까지만 가져오는걸로...
            foreach (AcctInfoDto acctInfo in commonInfoDto.acctInfoDtoList)
            {
                queryExecutor.InputDataTable.Clear();
                queryExecutor.InputDataTable["RecCnt"] = "1";
                queryExecutor.InputDataTable["QryTp"] = "0";
                queryExecutor.InputDataTable["AcntNo"] = acctInfo.accountNum;
                queryExecutor.InputDataTable["Pwd"] = acctInfo.accountPwd;
                queryExecutor.InputDataTable["QrySrtDt"] = DateTime.Today.AddDays(-70).ToString("yyyyMMdd");
                queryExecutor.InputDataTable["QryEndDt"] = DateTime.Today.ToString("yyyyMMdd");

                queryExecutor.InputDataTable["SrtNo"] = "";
                queryExecutor.InputDataTable["PdptnCode"] = "";
                queryExecutor.InputDataTable["IsuLgclssCode"] = "";
                queryExecutor.InputDataTable["IsuNo"] = "";
                
                queryExecutor.startTr("CDPCQ04700");
            }
        }

        public void updateTradeHistoryPriceInfo()
        {
            //trade history에서 end price가 널인 값중 가장 작은값을 조사..해서
            // 그 날짜부터 update..

            IList<string> stockCodeList = DaoFactory.getInstance.QueryForList<string>("selectStockTradeHistoryDistinct", null);
            foreach (string stockCode in stockCodeList)
            {
                queryExecutor.InputDataTable.Clear();
                queryExecutor.InputDataTable["shcode"] = stockCode;
                queryExecutor.InputDataTable["dwmcode"] = "1";
                queryExecutor.InputDataTable["idx"] = "";
                queryExecutor.InputDataTable["cnt"] = "200";
                
                IList<string> ymdDayList = DaoFactory.getInstance.QueryForList<string>("selectStockTradeHistoryPriceIsNull", null);
                foreach (string ymdDay in ymdDayList)
                {
                    queryExecutor.InputDataTable["date"] = ymdDay;


                    queryExecutor.startTr("t1305");
                }

            }
            
            

        }

        /*
         * 주식 계좌 기간별 수익률 조회하기 위한 서비스
         */
        public void insertProfitLossRateByDay()
        {
            foreach (AcctInfoDto acctInfo in commonInfoDto.acctInfoDtoList)
            {
                queryExecutor.InputDataTable.Clear();
                queryExecutor.InputDataTable["RecCnt"] = "1";
                queryExecutor.InputDataTable["AcntNo"] = acctInfo.accountNum;
                queryExecutor.InputDataTable["Pwd"] = acctInfo.accountPwd;

                string maxDay = DaoFactory.getInstance.QueryForObject<string>("selectStockAccountHistoryMaxYmdDay", new StockAccountHistoryDto(acctInfo.accountNum));
                if (maxDay == null)
                {
                    queryExecutor.InputDataTable["QrySrtDt"] = "20140430"; // 초기값.
                }
                else
                {
                    if (DateUtil.today().Equals(maxDay))
                        continue;

                    queryExecutor.InputDataTable["QrySrtDt"] = maxDay;
                }              
                
                queryExecutor.InputDataTable["QryEndDt"] = DateUtil.today();
                queryExecutor.InputDataTable["TermTp"] = "1";

                queryExecutor.startTr("FCOCCQ33600");
            }

        }

        /*
         * 현재 계좌정보를 STOCK_INFO에 저장하고, 
         * 현재 계좌는 NOTE를 CURRENT라고 주어서, 실시간 모니터링 대상에 포함시킨다.
         */
        public void upsertAccountInfo()
        {
            foreach(AcctInfoDto acctInfo in commonInfoDto.acctInfoDtoList) 
            {
                queryExecutor.InputDataTable.Clear();

                queryExecutor.InputDataTable["accno"] = acctInfo.accountNum;
                queryExecutor.InputDataTable["passwd"] = acctInfo.accountPwd;

                queryExecutor.startTr("t0424");
            }   
        }

        public void getCurBizDayContractStrength()
        {
            queryExecutor.startTr("t1310");
        }

    }
}
