﻿DROP TABLE STOCK_CONT_STR_TIME;
create table STOCK_CONT_STR_TIME
(
STOCK_CODE VARCHAR(8),
YMD_DAY varchar(8),
CONTRACT_TIME varchar(6),
CUR_PRICE mediumint,
FLUCTUATION decimal(9,2),
CONTRACT_STRENGTH decimal(9,2),
VOLUME int
);
CREATE INDEX STOCK_CONT_STR_TIME_IDX ON STOCK_CONT_STR_TIME ( STOCK_CODE, YMD_DAY); 
DROP TABLE STOCK_INFO;
create table STOCK_INFO
(
STOCK_CODE VARCHAR(8) NOT NULL PRIMARY KEY
,STOCK_NAME VARCHAR(30)
,ETF_GUBUN CHAR(1)
,GUBUN CHAR(1)
,PRICE INT
);
drop table STOCK_SIGNAL;
create table STOCK_SIGNAL
(
STOCK_CODE VARCHAR(8)
,CONTRACT_TIME varchar(6)
,CUR_PRICE mediumint
,FLUCTUATION DECIMAL(9,2)
,CONTRACT_STRENGTH decimal(9,2)
,CUR_TIME_VOLUME INT
,BEFORE_TIME_VOLUME INT
,SIGNAL_NAME VARCHAR(50)
,MORE_INFO VARCHAR(200)
);