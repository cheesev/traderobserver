﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    class StockAccountHistoryDto
    {
        public string accountNum { get; set; }
        public string ymdDay { get; set; }
        public string beginingNetAssets { get; set; }
        public string endingNetAssets { get; set; }
        public string investedAverageBalance { get; set; }
        public string returnOnInvestment { get; set; }

        public string depositAmt { get; set; }
        public string withdrawalAmt { get; set; }

        public StockAccountHistoryDto()
        {

        }
        public StockAccountHistoryDto(string accountNum)
        {
            this.accountNum = accountNum;
        }
    }
}
