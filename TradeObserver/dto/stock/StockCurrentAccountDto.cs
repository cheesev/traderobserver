﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    class StockCurrentAccountDto
    {
        public string stockCode { get; set; }

        public string accountNum { get; set; }
        public int balanceAmount { get; set; }
        public int avgPrice { get; set; }
        public int buyPrice { get; set; }
        public int currentPrice { get; set; }
        public int assessmentPrice { get; set; }
        public int assessmentProfitLoss { get; set; }
        public int profitLossRate { get; set; }
        public double investmentProportion { get; set; }

        public StockCurrentAccountDto()
        {

        }
    }
}
