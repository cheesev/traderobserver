﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    public class StockInfoDto
    {
        public string stockCode { get; set; }
        public string stockName { get; set; }
        public string etfgubun { get; set; } // 1 : ETF
        public string gubun { get; set; } // 1: KOSPI 2: KODAQ
        public int price { get; set; }
        public string note { get; set; }
        public StockInfoDto()
        {
        }

        public StockInfoDto(string stockCode, string stockName, string etfgubun, string gubun, int price)
        {
            this.stockCode = stockCode;
            this.stockName = stockName;
            this.etfgubun = etfgubun;
            this.gubun = gubun;
            this.price = price;
        }

        public override string ToString()
        {
            return this.stockCode + " , " + this.stockName + " , ETF? (1:ETF) : " + this.etfgubun + " , (1:KOSPI 2: KOSDAQ) : " + this.gubun;
        }
    }
}
