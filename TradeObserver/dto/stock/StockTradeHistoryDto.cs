﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    class StockTradeHistoryDto
    {
        public string ymdDay { get; set; }
        public int tradeNum { get; set; }      // 몇번째 거래인지..
        public string stockCode { get; set; }
        public string tradeType { get; set; }       // 매수, 매도 , 입금 구분..
        public int unitPrice { get; set; }     // 거래 단가.. 현재가 얼마에 거래했는지.
        public int amount { get; set; }        // 수량
        public int totalCost { get; set; }     // 거래 총 금액

        public int tradeDayEndPrice { get; set; }
        public int tradeDayLowPrice { get; set; }
        public int tradeDayHighPrice { get; set; }

        public StockTradeHistoryDto()
        {

        }
    }
}
