﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    public class StockPriceInfoDto
    {
        public string ymdDay { get; set; }
        public string stockCode { get; set; }
        
        public int endPrice { get; set; }
        public int lowPrice { get; set; }
        public int highPrice { get; set; }

        public int marketValue { get; set; }

        public StockPriceInfoDto()
        {
        }
    }
}
