﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    public class StockTradeSituationByDayDto
    {
        public string ymdDay { get; set; }
        public string stockCode { get; set; }
        public int price { get; set; }
        public double diff { get; set; }
        public int ant { get; set; }
        public int foreigner { get; set; }
        public int institution { get; set; }
        public int financialInvestment { get; set; }
        public int trust { get; set; }
        public int assurance { get; set; }
        public int bank { get; set; }
        public int investmentBank { get; set; }
        public int pension { get; set; }
        public int privateEquityFund { get; set; }
        public int innerForeigner { get; set; }
        public int etc { get; set; }
    }
}
