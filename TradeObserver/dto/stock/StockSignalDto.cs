﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    public class StockSignalDto
    {
        public string stockCode { get; set; }
        public string contractTime { get; set; }
        public long curPrice { get; set; }
        public double fluctuation { get; set; }
        public double contractStrength { get; set; }
        public long curTimeVolume { get; set; }
        public long beforeTimeVolume { get; set; }
        public string signalName { get; set; }
        public string moreInfo { get; set; }
        public StockSignalDto()
        {

        }
        public StockSignalDto(string stockCode, string contractTime, long curPrice, double fluctuation,
            double contractStrength, long curTimeVolume, long beforeTimeVolume, string signalName, string moreInfo)
        {
            this.stockCode = stockCode;
            this.contractTime = contractTime;
            this.curPrice = curPrice;
            this.fluctuation = fluctuation;
            this.contractStrength = contractStrength;
            this.curTimeVolume = curTimeVolume;
            this.beforeTimeVolume = beforeTimeVolume;
            this.signalName = signalName;
            this.moreInfo = moreInfo;
        }

        public override string ToString()
        {
            return this.stockCode + " , time : " + this.contractTime + " , price : " + this.curPrice + ", flunctuation : " + this.fluctuation + " , volume : " + curTimeVolume;
        }
    }
}
