﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.dto.stock
{
    //시간별 체결강도
    public class ContractStrengthByTimeDto
    {
        public String stockCode { get; set; }
        public String ymdDay { get; set; }
        public String contractTime { get; set; }
        public int curPrice { get; set; }
        public double fluctuation { get; set; }
        public double contractStrength { get; set; }
        public int volume { get; set; }
        public override string ToString()
        {
            return "stock code : " + this.stockCode + " , ymd day : " + this.ymdDay + "contract time : " + this.contractTime;
        }
    }
}
