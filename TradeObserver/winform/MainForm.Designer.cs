﻿namespace TradeObserver.winform
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kosdqReal = new System.Windows.Forms.Button();
            this.kospiReal = new System.Windows.Forms.Button();
            this.tradeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // kosdqReal
            // 
            this.kosdqReal.Location = new System.Drawing.Point(12, 12);
            this.kosdqReal.Name = "kosdqReal";
            this.kosdqReal.Size = new System.Drawing.Size(108, 23);
            this.kosdqReal.TabIndex = 0;
            this.kosdqReal.Text = "KOSDAQ_REAL";
            this.kosdqReal.UseVisualStyleBackColor = true;
            this.kosdqReal.Click += new System.EventHandler(this.kosdqReal_Click);
            // 
            // kospiReal
            // 
            this.kospiReal.Location = new System.Drawing.Point(165, 12);
            this.kospiReal.Name = "kospiReal";
            this.kospiReal.Size = new System.Drawing.Size(107, 25);
            this.kospiReal.TabIndex = 1;
            this.kospiReal.Text = "KOSPI_REAL";
            this.kospiReal.UseVisualStyleBackColor = true;
            this.kospiReal.Click += new System.EventHandler(this.kospiReal_Click);
            // 
            // tradeBtn
            // 
            this.tradeBtn.Location = new System.Drawing.Point(12, 60);
            this.tradeBtn.Name = "tradeBtn";
            this.tradeBtn.Size = new System.Drawing.Size(108, 24);
            this.tradeBtn.TabIndex = 2;
            this.tradeBtn.Text = "Trade";
            this.tradeBtn.UseVisualStyleBackColor = true;
            this.tradeBtn.Click += new System.EventHandler(this.tradeBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.tradeBtn);
            this.Controls.Add(this.kospiReal);
            this.Controls.Add(this.kosdqReal);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button kosdqReal;
        private System.Windows.Forms.Button kospiReal;
        private System.Windows.Forms.Button tradeBtn;
    }
}