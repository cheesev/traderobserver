﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeObserver.common.dto;
using TradeObserver.common.factory;
using TradeObserver.common.message;
using TradeObserver.dto.stock;
using TradeObserver.strategy.contract;
using TradeObserver.xingApi;

namespace TradeObserver.winform
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Dictionary<string, string> inputTable = new Dictionary<string, string>();

            XingFactory.getInstance.login();
            //XingFactory.getInstance.getRealInterestStock(); 
            
           // XingFactory.getInstance.updateAllStockInfo();
            //XingFactory.getInstance.insertAllStockTradeSituationByDay();



            //XingFactory.getInstance.insertStockTradeSituationByDay("000050");
        }
        private void kosdqReal_Click(object sender, EventArgs e)
        {
            XingFactory.getInstance.login();
            //XingFactory.getInstance.getRealContractDataByMarket(1);
        }

        private void kospiReal_Click(object sender, EventArgs e)
        {
            //XingFactory.getInstance.getRealContractDataByMarket(2);
        }

        private void tradeBtn_Click(object sender, EventArgs e)
        {
            //XingFactory.getInstance.insertStockTradeSituationByDay("005930");

            //XingFactory.getInstance.buyStock("114800", "7590", "10");
            //Simulation
            /*
            double totCnt = 0;
            double sucCnt = 0;
            double result = 0;

            IDictionary<string, string> inputDic = new Dictionary<string, string>();
            inputDic.Add("etfgubun", "0");

            IList<string> stockCodeList = DaoFactory.getInstance.QueryForList<string>("selectStockInfo", inputDic);

            foreach(string stockCode in stockCodeList)
            {
                Debug.WriteLine("processing : " + stockCode);
                inputDic["stockCode"] = stockCode;
                IList<StockSignalDto> stockSignalDtoList = DaoFactory.getInstance.QueryForList<StockSignalDto>("selectNTickStockSignal", inputDic);

                for (int i = 0; i < stockSignalDtoList.Count ; i++)
                {
                    //result = ContractStrengthStgy.dramaticIncreaseInNTickSimul(stockSignalDtoList, i, 10);
                    result = ContractStrengthStgy.dramaticIncreaseInNTickSimul(stockSignalDtoList.Reverse().ToList<StockSignalDto>(), i, 10);
                    if (result >=2 )
                    {
                        totCnt++;
                        sucCnt++;
                    }
                    else if (result != -100)
                    {
                        totCnt++;
                    }
                }
            }

            Debug.WriteLine("end trade" + "totCnt : " + totCnt + "sucCnt " + sucCnt + " reuslt : " + (sucCnt / totCnt));
            */
        }

        private void t1310Btn_Click(object sender, EventArgs e)
        {
            //t1702 test
            XingFactory.getInstance.insertStockTradeSituationByDay("005930");
        }

        
    }
}
