﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.common.factory;
using TradeObserver.common.message;
using TradeObserver.dto.stock;

namespace TradeObserver.strategy.contract
{
    class ContractStrengthStgy
    {
        public static void dramaticIncreaseInNTick(StockSignalDto stockSignalDto, int tickCnt)
        {
            IDictionary<string, string> inputDic = new Dictionary<string, string>();
            inputDic.Add("stockCode", stockSignalDto.stockCode);
            inputDic.Add("contractTime", stockSignalDto.contractTime );
            inputDic.Add("maxLimit", (tickCnt + 1).ToString());

            if( stockSignalDto.curTimeVolume > stockSignalDto.beforeTimeVolume * 3
                && stockSignalDto.curTimeVolume > 10000
                && stockSignalDto.contractStrength > 100
                && stockSignalDto.curPrice > 2000 
                && stockSignalDto.fluctuation <= 5 )
            {
                IList<StockSignalDto> stockContractList_NTick = DaoFactory.getInstance.QueryForList<StockSignalDto>("selectNTickStockSignal", inputDic);


                if (stockContractList_NTick.Count > tickCnt)
                {
                    if (stockContractList_NTick.ElementAt(1).contractStrength - stockSignalDto.contractStrength > 15
                    && stockContractList_NTick.ElementAt(5).contractStrength - stockSignalDto.contractStrength > 20
                    && stockContractList_NTick.ElementAt(10).contractStrength - stockSignalDto.contractStrength > 50)
                    {
                        //Debug.WriteLine("dramaticIncreaseInNTick condition meet");

                        //XingFactory.getInstance.buyStock(stockSignalDto.stockCode, stockSignalDto.curPrice.ToString(), "10");
                        XingFactory.getInstance.addStockSignalList(stockSignalDto, "gradually increase");
                    }
                }
            }
        }

        public static double dramaticIncreaseInNTickSimul(IList<StockSignalDto> stockSignalDtoList, int idx, int maxTickCnt)
        {
            StockSignalDto stockSignalDto = stockSignalDtoList.ElementAt(idx);

            if (stockSignalDto.curTimeVolume > stockSignalDto.beforeTimeVolume * 4
                && stockSignalDto.beforeTimeVolume > 10000
                && stockSignalDto.contractStrength > 100
                && stockSignalDto.curPrice > 5000
                && stockSignalDto.fluctuation <= 5
                && int.Parse(stockSignalDto.contractTime) < 130000)
            {

                if ( idx >= maxTickCnt )
                {
                    if ( stockSignalDto.contractStrength - stockSignalDtoList.ElementAt(idx - 1).contractStrength > 15
                    && stockSignalDto.contractStrength - stockSignalDtoList.ElementAt(idx - (maxTickCnt / 5)).contractStrength > 20
                    && stockSignalDto.contractStrength - stockSignalDtoList.ElementAt(idx - maxTickCnt).contractStrength > 50)
                    {
                        //Debug.WriteLine("dramaticIncreaseInNTick condition meet");

                        //XingFactory.getInstance.buyStock(stockSignalDto.stockCode, stockSignalDto.curPrice.ToString(), "10");
                        return  XingFactory.getInstance.addStockSignalList(stockSignalDto, "gradually increase");
                        //사고 바로 나가버림.. 더이상 할필요없음..
                    }
                }
            }
            return -100;
        }

    }
}
