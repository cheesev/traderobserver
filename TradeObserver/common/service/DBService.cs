﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.common.message;

namespace TradeObserver.common.service
{
    class DBService
    {

        public static void insertTransaction<T>(string queryName, List<T> list)
        {
            try
            {
                DaoFactory.getInstance.BeginTransaction();
                foreach (T item in list)
                {
                    DaoFactory.getInstance.Insert(queryName, item);
                }
                DaoFactory.getInstance.CommitTransaction();
                Console.WriteLine("commit success .. count : " + list.Count);
            }
            catch(Exception e)
            {
                Console.WriteLine("error occur : rollback");
                Console.WriteLine("index 0 element : " + list.ElementAt(0).ToString());
                //MyPeopleMessage.sendMsg("error : index 0 element : " + list.ElementAt(0).ToString());
                Console.WriteLine(e.ToString());
                DaoFactory.getInstance.RollBackTransaction();
            }

        }

        public static void updateTransaction<T>(string queryName, List<T> list)
        {
            try
            {
                DaoFactory.getInstance.BeginTransaction();
                foreach (T item in list)
                {
                    DaoFactory.getInstance.Update(queryName, item);
                }
                DaoFactory.getInstance.CommitTransaction();
                Console.WriteLine("commit success .. count : " + list.Count);
            }
            catch (Exception e)
            {
                Console.WriteLine("error occur : rollback");
                Console.WriteLine("index 0 element : " + list.ElementAt(0).ToString());
                //MyPeopleMessage.sendMsg("error : index 0 element : " + list.ElementAt(0).ToString());
                Console.WriteLine(e.ToString());
                DaoFactory.getInstance.RollBackTransaction();
            }

        }

    }
}
