﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.dto.stock;

namespace TradeObserver.common.dto
{
    class CommonInfoDto
    {
        public string server { get; set; }

        public IList<AcctInfoDto> acctInfoDtoList { get; set; }
        public UserInfoDto userInfoDto { get; set; }
        public string currentOpName { get; set; }

        public Dictionary<string, StockSignalDto> stockSignalList { get; set; }
    }
}
