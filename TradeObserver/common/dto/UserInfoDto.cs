﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.dto.stock;

namespace TradeObserver.common.dto
{
    public class UserInfoDto
    {
        public string loginId { get; set; }
        public string loginPwd { get; set; }
        public string pkiPwd { get; set; }
    }
}
