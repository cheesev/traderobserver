﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.dto.stock;

namespace TradeObserver.common.dto
{
    public class AcctInfoDto
    {
        public string accountNum { get; set; }
        public string accountName { get; set; }
        public string accountPwd { get; set; }

        public AcctInfoDto()
        {

        }
    }
}
