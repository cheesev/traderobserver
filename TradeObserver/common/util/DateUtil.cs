﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.common.util
{
    class DateUtil
    {
        /*
         * DateTime에서 HH 를 써줘야하는것 같고.. TimeSpan은 hh인듯..
         * 
         */
        public static string substractTime(string stndTime, string subTime)
        {
            TimeSpan stndTp = DateTime.ParseExact(stndTime, "HHmmss", System.Globalization.CultureInfo.InvariantCulture).TimeOfDay;
            TimeSpan subTp = DateTime.ParseExact(subTime, "HHmmss", System.Globalization.CultureInfo.InvariantCulture).TimeOfDay;

            return stndTp.Subtract(subTp).ToString("hhmmss");

        }

        public static string today()
        {
            //return DateTime.ParseExact("11", "yyyyMMdd").AddDays(1);
            return DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
        }
        public static string addDay(string ymdDay, int value)
        {
            DateTime ymdDayTime = DateTime.ParseExact(ymdDay,
                                        "yyyyMMdd",
                                        System.Globalization.CultureInfo.InvariantCulture,
                                        System.Globalization.DateTimeStyles.None);

            return ymdDayTime.AddDays(value).ToString("yyyyMMdd");
        }


    }
}
