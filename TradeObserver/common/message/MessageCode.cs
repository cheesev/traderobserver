﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeObserver.common.message
{
    class MessageCode
    {
        public static readonly string TGRAM_MSG_QUEUE = "msg_queue";
        public static readonly string TGRAM_OVER_VARIABILITY = "기준치 이상 변동";
    }
}
