﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TradeObserver.common.message
{
    class MyPeopleMessage
    {
        public static void sendMsg(String text)
        {
            // WebRequest 객체 생성
            WebRequest m_wrRequest;
            HttpWebResponse m_hwrResponse;
            List<String> m_alPostData;
            m_wrRequest = WebRequest.Create("https://apis.daum.net/mypeople/buddy/send.xml");
            m_wrRequest.Method = "POST";
            m_wrRequest.ContentType = "application/x-www-form-urlencoded";
            m_alPostData = new List<String>();
            m_alPostData.Clear();
            m_alPostData.Add("apikey=04f241a81946c2a9280f62caa7a83565180cbafb");
            m_alPostData.Add("content=" + HttpUtility.UrlEncode(text, Encoding.GetEncoding("UTF-8")));
            m_alPostData.Add("buddyId=BU_NyPS31hzBc7N1g-.WnNohQ00");

            // POST 파라미터 설정. "&" 문자로 전송할 데이터를 조합 하는 부분 입니다
            String sParameters = String.Join("&", (String[])m_alPostData.ToArray());
            m_wrRequest.ContentLength = sParameters.Length;
            StreamWriter swWriter = null;
            Stream resPostStream = null;
            StreamReader srReader = null;
            // 스트림을 통해 웹서버로 POST
            swWriter = new StreamWriter(m_wrRequest.GetRequestStream());
            swWriter.Write(sParameters);
            swWriter.Close();

            // 웹서버로부터 응답받기
            m_hwrResponse = (HttpWebResponse)m_wrRequest.GetResponse();
            resPostStream = m_hwrResponse.GetResponseStream();
            srReader = new StreamReader(resPostStream, Encoding.Default);

            // 스트림을 문자열로 변환
            String m_sResultPost = srReader.ReadToEnd();
            System.Console.Out.WriteLine(m_sResultPost);
            resPostStream.Close();
            srReader.Close();

            m_wrRequest = null;
            swWriter = null;
            resPostStream = null;
            srReader = null;
        }
    }
}
