﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookSleeve;

namespace TradeObserver.common.factory
{
    class RedisFactory
    {
        private static RedisConnection redisConn = null;
        public const string telegramKey = "telegram";
        public static RedisConnection getInstance
        {
            get
            {
                try
                {
                    if (redisConn == null)
                    {
                        redisConn = new RedisConnection("192.168.56.101");
                        redisConn.Open();
                    }
                }
                catch
                {
                    throw;
                }
                return redisConn;
            }
        }
    }
}
