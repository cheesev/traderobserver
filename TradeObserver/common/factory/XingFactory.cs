﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeObserver.xingApi;

namespace TradeObserver.common.factory
{
    class XingFactory
    {
        private static object syncLock = new object();
        private static XingApiCustomService xingApiCustomService = null;

        public static XingApiCustomService getInstance
        {
            get
            {
                try
                {
                    if (xingApiCustomService == null)
                    {
                        lock (syncLock)
                        {
                            if (xingApiCustomService == null)
                            {
                                xingApiCustomService = new XingApiCustomService();
                            }
                        }
                    }
                }
                catch
                {
                    throw;
                }
                return xingApiCustomService;
            }
        }
    }
}
