﻿using System.Xml;
using IBatisNet.Common.Utilities;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;

public class DaoFactory
{
    private static object syncLock = new object();
    private static ISqlMapper mapper = null;

    public static ISqlMapper getInstance
    {
        get
        {
            try
            {
                if (mapper == null)
                {
                    lock (syncLock)
                    {
                        if (mapper == null)
                        {
                            DomSqlMapBuilder dom = new DomSqlMapBuilder();
                            mapper = dom.Configure(@"../../iBatis/SqlMap.config");
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return mapper;
        }
    }
}